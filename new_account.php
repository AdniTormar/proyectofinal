<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();

session_start();
session_abort();
if(isset($_REQUEST["mensaje"]))
    $mensaje=$_REQUEST["mensaje"];
else
    $mensaje="";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
</head>
<body onmouseover="CambiarCur()" id="body">
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
        include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>
<div class="container">
    <form class="form-signin centrar" method="post" action="crear.php" enctype="multipart/form-data">
        <div class="perfil">
            <h2 class="form-signin-heading" align="center">Perfil</h2>
            <br>
            <label for="inputEmail" class="sr-only">Imagen_Perfil</label>
            <input type="file" id="imagenP" name="imagenP" class="form-control" placeholder="Imagen" required autofocus>
        </div>
        <h2 class="form-signin-heading" align="center">Crear Cuenta</h2>
        <br>
        <label for="inputEmail" class="sr-only">Apodo</label>
        <input type="text" id="apodo" name="apodo" class="form-control" placeholder="Apodo" maxlength="7" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Imagen_Usuario</label>
        <input type="file" id="imagen" name="imagen" class="form-control" placeholder="Imagen" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Usuario</label>
        <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuario" maxlength="6" minlength="5" required autofocus>
        <br>
        <label for="inputPassword" class="sr-only">Contraseña</label>
        <input type="password" id="contrasena" name="contrasena" class="form-control" placeholder="Contraseña" maxlength="15" minlength="8" required autofocus>
        <br>
        <label for="inputPassword" class="sr-only">Contraseña</label>
        <input type="password" id="contrasena2" name="contrasena2" class="form-control" placeholder="Confirmar Contraseña" maxlength="15" minlength="8" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Nombre</label>
        <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Apellidos</label>
        <input type="text" id="apellidos" name="apellidos" class="form-control" placeholder="Apellidos" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Telefono</label>
        <input type="text" id="telefono" name="telefono" class="form-control" placeholder="Telefono" maxlength="9" minlength="9"  required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Direccion</label>
        <input type="text" id="direccion" name="direccion" class="form-control" placeholder="Dirección" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Correo</label>
        <input type="text" id="correo" name="correo" class="form-control" placeholder="Correo" required autofocus>
        <br>
        <input type="hidden" name="perfil" id="perfil" value="3">
        <?php
        if(isset($_SESSION["Admin"])||isset($_SESSION["AdminCo"])){
        ?>
            <select class="form-control" name="perfil" id="perfil" style="width: 200px;height: 50px" required autofocus>
                <?php
                if(isset($_SESSION["Admin"])||isset($_SESSION["AdminCo"])){
                ?>
                <option selected value="1">Administrador
                    <?php
                    }
                    ?>
                    <?php
                    if(isset($_SESSION["Admin"])||isset($_SESSION["AdminCo"])){
                    ?>
                <option value="2">Editor
                    <?php
                    }
                    ?>
                <option value="3">Usuario
        </select>
            <?php
        }
        ?>
        <br>
        <button class="btn btn-lg btn-primary btn-block centrar" type="submit">Enviar</button>
        <br>
        <?php
        if($mensaje != ""){
            ?>
            <div class="alert alert-danger centrar" align="center">
                <?=base64_decode($mensaje) ?>
            </div>
            <?php
        }
        ?>
    </form>
</div> <!-- /container -->
<div "container">
    <div align="center" class="abajo2">
        <?php
            include("pie.php");
        ?>
    </div>
</div>
<?php
$db->desconectar();
?>
</body>
</html>