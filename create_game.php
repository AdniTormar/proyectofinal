<?php
session_start();
if(!(isset($_SESSION["Admin"])||isset($_SESSION["Editor"])||isset($_SESSION["AdminCo"])||isset($_SESSION["EditorCo"]))){
    header("loaction: index.php");
}
?>
<?php
include("conf/configuracion.php");
include("funcion/funcion.php");

$db=new Db();
$db->conectar();

$titulo = $_POST["titulo"];
$descripcion = $_POST["descripcion"];
$precio = $_POST["precio"];
$empresa = $_POST["empresa"];
$fecha = date("Y-m-d", strtotime($_POST["fecha"]));

$foto1 = $_FILES["foto1"]["name"];
$tmp_foto1 = $_FILES["foto1"]["tmp_name"];

$foto2 = $_FILES["foto2"]["name"];
$tmp_foto2 = $_FILES["foto2"]["tmp_name"];

$video = $_FILES["video"]["name"];
$tmp_video = $_FILES["video"]["tmp_name"];

$genero = $_POST["genero"];
$edad = $_POST["edad"];
$plataforma= $_POST["plataforma"];
$dlc=$_POST["dlc"];
$disponible = $_POST["disponible"];

if(isset($_POST["disponible"])){
    $disponible=true;
}
else{
    $disponible=false;
}
if(isset($_POST["dlc"])){
    $dlc=true;
}
else{
    $dlc=false;
}

if(isset($_SESSION["id"])){
    $foto1Separar=explode(".",$foto1);
    $foto1=$foto1Separar[0]."-".$_SESSION["id"].".".$foto1Separar[1];

    $foto2Separar=explode(".",$foto2);
    $foto2=$foto2Separar[0]."-".$_SESSION["id"].".".$foto2Separar[1];

    $videoSeparar=explode(".",$video);
    $video=$videoSeparar[0]."-".$_SESSION["id"].".".$videoSeparar[1];

    $ruta_foto1="img/".$foto1;
    move_uploaded_file($tmp_foto1, $ruta_foto1);

    $ruta_foto2="img/".$foto2;
    move_uploaded_file($tmp_foto2, $ruta_foto2);

    $ruta_video="videos/".$video;
    move_uploaded_file($tmp_video, $ruta_video);
}
else if(isset($_SESSION["idCo"])){
    $foto1Separar=explode(".",$foto1);
    $foto1=$foto1Separar[0]."-".$_SESSION["idCo"].".".$foto1Separar[1];

    $foto2Separar=explode(".",$foto2);
    $foto2=$foto2Separar[0]."-".$_SESSION["idCo"].".".$foto2Separar[1];

    $videoSeparar=explode(".",$video);
    $video=$videoSeparar[0]."-".$_SESSION["idCo"].".".$videoSeparar[1];

    $ruta_foto1="img/".$foto1;
    move_uploaded_file($tmp_foto1, $ruta_foto1);

    $ruta_foto2="img/".$foto2;
    move_uploaded_file($tmp_foto2, $ruta_foto2);

    $ruta_video="videos/".$video;
    move_uploaded_file($tmp_video, $ruta_video);
}

if($titulo==null ||$descripcion==null || $precio==null || $empresa==null || $fecha==null || $foto1==null || $tmp_foto1 ==null || $foto2==null || $tmp_foto2 ==null ||$plataforma==null||(!($dlc==true||$dlc==false)) || (!($disponible==true||$disponible==false))|| $genero==null||$edad==null){
    $db->desconectar();
    header("location: index.php");
}
$sql="insert into juegos (titulo,descripcion,precio,genero,empresa,pegi,fecha,foto_ver,foto_fondo,video,dlc,plataforma,disponible) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
$resultado=$db->lanzar_consulta($sql, array($titulo,$descripcion,$precio,$genero,$empresa,$edad,$fecha,$foto1,$foto2,$video,$dlc,$plataforma,$disponible));

$db->desconectar();

header("location: new_game.php");
?>