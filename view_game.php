<?php

define("TAMANO_PAGINA", 12);
$contador=0;
if(isset($_REQUEST["pagina"])){
    $pagina=$_REQUEST["pagina"];
}
else{
    $pagina = 0;
}


$sql3="select COUNT(*) as 'cantidad' from juegos where disponible=?";
$resultado3=$db->lanzar_consulta($sql3,array(1));
$fila3=$resultado3->fetch_assoc();
$entradas=$fila3["cantidad"];
$paginas=$entradas / TAMANO_PAGINA;

$sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver,id,disponible,plataforma,dlc FROM juegos where disponible=?  LIMIT " . $pagina  * TAMANO_PAGINA . ", " . TAMANO_PAGINA;
$resultado=$db->lanzar_consulta($sql,array(1));
while($fila=$resultado->fetch_assoc()){
    ?>
    <?php
    $contador++;
    ?>
    <?php
    if($contador==13) {
        $contador=0;
    }
    if($contador<=4) {
        ?>
        <td>
            <div>
                <div class="card juego">
                    <img class="card-img-top ver" src="img/<?= $fila['foto_ver'] ?>" alt="Card image cap">
                    <?php
                    if($fila["plataforma"]=="steam"){
                        ?>
                        <img class="card-img-top ver2" src="img/steam.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="origin"){
                        ?>
                        <img class="card-img-top ver2" src="img/origin.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="uplay"){
                        ?>
                        <img class="card-img-top ver2" src="img/uplay.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="battle.net"){
                        ?>
                        <img class="card-img-top ver2" src="img/battle.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="ps4"){
                        ?>
                        <img class="card-img-top ver2" src="img/ps4.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="nintendo"){
                        ?>
                        <img class="card-img-top ver2" src="img/nintendo.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["dlc"]==1){
                        ?>
                        <img class="card-img-top ver3" src="img/dlc.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <div class="card-body">
                        <h4 class="card-title"><?= $fila["titulo"] ?></h4>
                        <p class="card-text"><?= $fila['descripcion'] ?>...</p>
                        <a href="?id=game&id_juego=<?= $fila["id"] ?><?= $fila["titulo"] ?>" class="btn btn-primary">Leer
                            más</a>
                    </div>
                </div>
            </div>
        </td>
        <?php
    }
    ?>
    <?php
    if($contador>=5) {
        ?>
        <td>
            <div>
                <div class="card <?php if($contador==5){?>juego2<?php } ?><?php if($contador==6){?>juego3<?php } ?><?php if($contador==7){?>juego4<?php } ?><?php if($contador==8){?>juego5<?php } ?><?php if($contador==9){?>juego6<?php } ?><?php if($contador==10){?>juego7<?php } ?><?php if($contador==11){?>juego8<?php } ?><?php if($contador==12){?>juego9<?php } ?>">
                    <img class="card-img-top ver" src="img/<?= $fila['foto_ver'] ?>" alt="Card image cap">
                    <?php
                    if($fila["plataforma"]=="steam"){
                        ?>
                        <img class="card-img-top ver2" src="img/steam.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="origin"){
                        ?>
                        <img class="card-img-top ver2" src="img/origin.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="uplay"){
                        ?>
                        <img class="card-img-top ver2" src="img/uplay.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="battle.net"){
                        ?>
                        <img class="card-img-top ver2" src="img/battle.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="ps4"){
                        ?>
                        <img class="card-img-top ver2" src="img/ps4.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["plataforma"]=="nintendo"){
                        ?>
                        <img class="card-img-top ver2" src="img/nintendo.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <?php
                    if($fila["dlc"]==1){
                        ?>
                        <img class="card-img-top ver3" src="img/dlc.png" alt="Card image cap">
                        <?php
                    }
                    ?>
                    <div class="card-body">
                        <h4 class="card-title"><?= $fila["titulo"] ?></h4>
                        <p class="card-text"><?= $fila['descripcion'] ?>...</p>
                        <a href="?id=game&id_juego=<?= $fila["id"] ?><?= $fila["titulo"] ?>"
                           class="btn btn-primary">Leer
                            más</a>
                    </div>
                </div>
            </div>
        </td>
        <?php
    }
}
?>
<nav class="<?php if($contador<=4 && $contador<5){?>paginacion3<?php } ?><?php if($contador>=5 && $contador<9){?>paginacion2<?php } ?><?php if($contador>=9){?>paginacion<?php } ?>" aria-label="Page navigation example">
    <ul class="pagination">
        <?php
        for ($i = 0;$i<$paginas;$i++){
            ?>
            <li class="page-item"><a class="page-link" href="?id=view_game&pagina=<?=$i ?>" > <?=$i + 1?> </a></li>
            <?php
        }
        ?>
        <li class="page-item"><a class="page-link" > ... </a></li>
    </ul>
</nav>

