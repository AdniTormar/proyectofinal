<?php

$accion="accion";
$arcade="arcade";
$aventura="aventura";
$carreras="carreras";
$cooperacion="cooperacion";
$multijugador="multijugador";
$deporte="deporte";
$estrategia="estrategia";
$fps="fps";
$indies="indies";
$lucha="lucha";
$mmo="mmo";
$simulacion="simulacion";
$rol="rol";
$misterio="misterio";
$rpg="rpg";
$mundo_abierto="mundo abierto";

$sqlA=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlA2=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlA3=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlC=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlC2=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlM=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlD=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlE=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlF=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlI=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlL=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlM2=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlS=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlR=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlM3=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlR2=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");
$sqlM4=("select COUNT(*)AS 'cantidad' from juegos where genero=? and disponible=?");

$resultadoA =$db->lanzar_consulta($sqlA, array($accion,1));
$resultadoA2 =$db->lanzar_consulta($sqlA2, array($arcade,1));
$resultadoA3 =$db->lanzar_consulta($sqlA3, array($aventura,1));
$resultadoC =$db->lanzar_consulta($sqlC, array($carreras,1));
$resultadoC2 =$db->lanzar_consulta($sqlC2, array($cooperacion,1));
$resultadoM=$db->lanzar_consulta($sqlM, array($multijugador,1));
$resultadoD =$db->lanzar_consulta($sqlD, array($deporte,1));
$resultadoE =$db->lanzar_consulta($sqlE, array($estrategia,1));
$resultadoF =$db->lanzar_consulta($sqlF, array($fps,1));
$resultadoI =$db->lanzar_consulta($sqlI, array($indies,1));
$resultadoL =$db->lanzar_consulta($sqlL, array($lucha,1));
$resultadoM2 =$db->lanzar_consulta($sqlM2, array($mmo,1));
$resultadoS =$db->lanzar_consulta($sqlS, array($simulacion,1));
$resultadoR =$db->lanzar_consulta($sqlR, array($rol,1));
$resultadoM3 =$db->lanzar_consulta($sqlM3, array($misterio,1));
$resultadoR2 =$db->lanzar_consulta($sqlR2, array($rpg,1));
$resultadoM4 =$db->lanzar_consulta($sqlM4, array($mundo_abierto,1));

?>
<ul class="list-group lateral">
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=accion">Acción</a>
        <span class="badge badge-primary badge-pill"><?php while($filaA=$resultadoA->fetch_assoc()){ if($filaA["cantidad"]==0){}else{echo $filaA["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=arcade">Arcade</a>
        <span class="badge badge-primary badge-pill"><?php while($filaA2=$resultadoA2->fetch_assoc()){ if($filaA2["cantidad"]==0){}else{echo $filaA2["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=aventura">Aventura</a>
        <span class="badge badge-primary badge-pill"><?php while($filaA3=$resultadoA3->fetch_assoc()){ if($filaA3["cantidad"]==0){}else{echo $filaA3["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=carrera">Carreras</a>
        <span class="badge badge-primary badge-pill"><?php while($filaC=$resultadoC->fetch_assoc()){ if($filaC["cantidad"]==0){}else{echo $filaC["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=cooperacion">Cooperación</a>
        <span class="badge badge-primary badge-pill"><?php while($filaC2=$resultadoC2->fetch_assoc()){ if($filaC2["cantidad"]==0){}else{echo $filaC2["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=deporte">Deporte</a>
        <span class="badge badge-primary badge-pill"><?php while($filaD=$resultadoD->fetch_assoc()){ if($filaD["cantidad"]==0){}else{echo $filaD["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=estrategia">Estrategia</a>
        <span class="badge badge-primary badge-pill"><?php while($filaE=$resultadoE->fetch_assoc()){ if($filaE["cantidad"]==0){}else{echo $filaE["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=fps">Fps</a>
        <span class="badge badge-primary badge-pill"><?php while($filaF=$resultadoF->fetch_assoc()){ if($filaF["cantidad"]==0){}else{echo $filaF["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=indies">Indies</a>
        <span class="badge badge-primary badge-pill"><?php while($filaI=$resultadoI->fetch_assoc()){ if($filaI["cantidad"]==0){}else{echo $filaI["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=lucha">Lucha</a>
        <span class="badge badge-primary badge-pill"><?php while($filaL=$resultadoL->fetch_assoc()){ if($filaL["cantidad"]==0){}else{echo $filaL["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=misterio">Misterio</a>
        <span class="badge badge-primary badge-pill"><?php while($filaM3=$resultadoM3->fetch_assoc()){ if($filaM3["cantidad"]==0){}else{echo $filaM3["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=mmo">Mmo</a>
        <span class="badge badge-primary badge-pill"><?php while($filaM2=$resultadoM2->fetch_assoc()){ if($filaM2["cantidad"]==0){}else{echo $filaM2["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=multijugador">Multijugador</a>
        <span class="badge badge-primary badge-pill"><?php while($filaM=$resultadoM->fetch_assoc()){ if($filaM["cantidad"]==0){}else{echo $filaM["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center lateral">
        <a href="games_lateral.php?genero=mundo abierto">Mundo Abierto</a>
        <span class="badge badge-primary badge-pill"><?php while($filaM4=$resultadoM4->fetch_assoc()){ if($filaM4["cantidad"]==0){}else{echo $filaM4["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=rol">Rol</a>
        <span class="badge badge-primary badge-pill"><?php while($filaR=$resultadoR->fetch_assoc()){ if($filaR["cantidad"]==0){}else{echo $filaR["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=rpg">Rpg</a>
        <span class="badge badge-primary badge-pill"><?php while($filaR2=$resultadoR2->fetch_assoc()){ if($filaR2["cantidad"]==0){}else{echo $filaR2["cantidad"];}} ?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="games_lateral.php?genero=simulacion">Simulación</a>
        <span class="badge badge-primary badge-pill"><?php while($filaS=$resultadoS->fetch_assoc()){ if($filaS["cantidad"]==0){}else{echo $filaS["cantidad"];}} ?></span>
    </li>
</ul>