<?php
include("conf/configuracion.php");
include("funcion/funcion.php");

$db=new Db();
$db->conectar();

$juego = $_POST["juego"];
$id_usuario = $_POST["id_usuario"];
$id_juego = $_POST["id_juego"];
$foto_ver = $_POST["foto_ver"];
$comentario = $_POST["comentario"];
$valoracion = $_POST["valoracion"];
$fecha_compra = $_POST["fecha_compra"];

if($juego==null||$id_usuario==null||$id_juego==null||$foto_ver==null||$valoracion==null||$fecha_compra==null){
    $db->desconectar();
    header("location: index.php");
}
else{
    $sql="insert into comentarios(juego, comentario, valoracion, fecha_compra,foto_ver,id_juego, id_usuario) VALUES (?,?,?,?,?,?,?)";
    $resultado=$db->lanzar_consulta($sql,array($juego,$comentario,$valoracion,$fecha_compra,$foto_ver,$id_juego,$id_usuario));

    $db->desconectar();
    header("location: comentarios.php");
}
?>