<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();
if(isset($_GET["foto_ver"])||isset($_GET["juego"])||isset($_GET["fecha_compra"])||isset($_GET["id_usuario"])||isset($_GET["id_juego"])){
    $juego=$_GET["juego"];
    $foto_ver=$_GET["foto_ver"];
    $fecha_compra=$_GET["fecha_compra"];
    $id_usuario=$_GET["id_usuario"];
    $id_juego=$_GET["id_juego"];
}
else{
    //header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
    <SCRIPT src="libs/ckeditor/ckeditor.js"></SCRIPT>
</head>
<body onmouseover="CambiarCur()" id="body">
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
    include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>
<div class="container" style="position: relative;top:70px;left:75px;">
    <form class="form-signin" method="post" action="new_comentario.php" enctype="multipart/form-data">
        <h2 class="form-signin-heading" align="center">Comentario</h2>
        <input type="hidden" id="id_juego" name="id_juego" value="<?=$id_juego?>">
        <input type="hidden" id="id_usuario" name="id_usuario" value="<?=$id_usuario?>">
        <input type="hidden" id="fecha_compra" name="fecha_compra" value="<?=$fecha_compra?>">
        <input type="hidden" id="foto_ver" name="foto_ver" value="<?=$foto_ver?>">
        <input type="hidden" id="juego" name="juego" value="<?=$juego?>">
        <br>
        <textarea name="comentario" class="form-control" id="comentario" placeholder="Descripcion" required autofocus></textarea>
        <br>
        <h2 class="form-signin-heading" align="center">Valoración</h2>
        <br>
        <div align="center">
            <select class="form-control" name="valoracion" id="valoracion" style="width: 200px;height: 50px" required autofocus>
                <option selected value="1">1
                <option value="2">2
                <option value="3">3
                <option value="4">4
                <option value="5">5
                <option value="6">6
                <option value="7">7
                <option value="8">8
                <option value="9">9
                <option value="10">10
            </select>
        </div>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit" STYLE="color: white!important;background-color: #007aff!important;">Enviar</button>
    </form>
</div>

<!-- Abajo -->
<div class="container">
    <div align="center" class="abajoCom">
        <?php
        include("pie.php");
        ?>
    </div>
</div>
<?php
$db->desconectar();
?>
<script>
    CKEDITOR.replace("comentario");
</script>
</body>
</html>
