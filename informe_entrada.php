<?php
require("libs/fpdf/fpdf.php");
require("conf/configuracion.php");
require("funcion/funcion.php");

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont("Times", 'B', 20);
$pdf->Cell(120, 10, "Informe de Usuarios Admin");
$pdf->SetFont("Times", 'B', 15);
$pdf->Cell(10, 10,"Fecha del informe: ". date("d-m-Y"));
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont("Times", '', 15);
$db = new Db();
$db->conectar();
$sql = "SELECT apodo, usuario, correo,telefono,tipo_usuario FROM usuarios WHERE tipo_usuario =? ";
$resultado = $db->lanzar_consulta($sql,array("Admin"));
while ($fila = $resultado->fetch_assoc()) {
    $apodo = $fila["apodo"];
    $usuario = $fila["usuario"];
    $telefono = $fila["telefono"];
    $correo = $fila["correo"];

    $pdf->Cell(40, 10, "Apodo: " .  $apodo .
        " | Usuario: " . $usuario . " | Telefono: " .$telefono .
        "|Correo: ". $correo);
    $pdf->Ln();
}
$db->desconectar();

$pdf->Output();