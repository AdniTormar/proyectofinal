<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();

$sqlC="select COUNT(*) as 'cantidad' from cesta,usuarios where cesta.id_usuario=usuarios.id and usuarios.id=?";
if(isset($_SESSION["id"])){
    $resultadoC=$db->lanzar_consulta($sqlC,array($_SESSION["id"]));
}
if(isset($_SESSION["idCo"])){
    $resultadoC=$db->lanzar_consulta($sqlC,array($_SESSION["idCo"]));
}
while($filaC=$resultadoC->fetch_assoc()){
    if($filaC["cantidad"]==0){
        header("Location: perfil.php?juegos_comprados=1");
    }
    else{

    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
</head>
<body onmouseover="CambiarCur()" id="body">
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
        include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>

<div class="div_jue">
    <table cellspacing="3" cellpadding="3">
        <tr>
        <?php
            include("view_cesta.php");
        ?>
        </tr>
    </table>
</div>
<?php
$sqlCom = "SELECT * FROM cesta,juegos,juegos_cesta,usuarios where cesta.id=juegos_cesta.id_cesta and juegos.id=juegos_cesta.id_juego and usuarios.id=cesta.id_usuario and disponible=? and usuarios.id=? LIMIT " . $pagina  * TAMANO_PAGINA . ", " . TAMANO_PAGINA;
if(isset($_SESSION["id"])) {
    $resultadoCom = $db->lanzar_consulta($sqlCom, array(1,$_SESSION["id"]));
}
if(isset($_SESSION["idCo"])) {
    $resultadoCom = $db->lanzar_consulta($sqlCom, array(1,$_SESSION["idCo"]));
}
?>
<div class="div_jue">
    <table cellspacing="3" cellpadding="3">
        <tr>
            <?php
            $contadorB=0;
            while($filaCom=$resultadoCom->fetch_assoc()) {
                $contadorB++;
                ?>
            <td class="<?php if($contadorB==1){ ?>comprar1<?php } ?><?php if($contadorB==2){ ?>comprar2<?php } ?><?php if($contadorB==3){ ?>comprar3<?php } ?><?php if($contadorB==4){ ?>comprar4<?php } ?><?php if($contadorB==5){ ?>comprar5<?php } ?><?php if($contadorB==6){ ?>comprar6<?php } ?><?php if($contadorB==7){ ?>comprar7<?php } ?><?php if($contadorB==8){ ?>comprar8<?php } ?><?php if($contadorB==9){ ?>comprar9<?php } ?><?php if($contadorB==10){ ?>comprar10<?php } ?><?php if($contadorB==11){ ?>comprar11<?php } ?><?php if($contadorB==12){ ?>comprar12<?php } ?>">
                <a class="btn btn-warning" href="comprar.php?juego=<?php echo $filaCom["juego"] ?>&foto_ver=<?php echo $filaCom["foto_ver"] ?>&id_cesta=<?php echo $filaCom["id_cesta"] ?>&id_usuario=<?php echo $filaCom["id_usuario"] ?>&id_juego=<?php echo $filaCom["id_juego"] ?>">Comprar</a>
            </td>
                <?php
            }
            ?>
        </tr>
    </table>
</div>
<!-- Abajo -->
<div class="container">
    <?php
    if(isset($contador)){
    ?>
    <div align="center" class="<?php if ($contador <= 4 && $contador < 5) { ?>abajo9<?php } ?><?php if ($contador >= 5 && $contador < 9) { ?>abajo7<?php } ?><?php if ($contador >= 9) { ?>abajo8<?php } ?>">
        <?php
        }
        if(!(isset($contador))) {
        ?>
        <div align="center" class="abajoJ">
            <?php
            }
            ?>
            <?php
            include("pie.php");
            ?>
        </div>
    </div>
<?php
$db->desconectar();
?>
</body>
</html>