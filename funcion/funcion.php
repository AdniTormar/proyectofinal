<?php
    class Db{
        private $conexion;
        function conectar(){
            $this->conexion=new mysqli(BD_HOST,BD_USUARIO,BD_PASSWORD,BD_NAME);
        }
        function desconectar(){
            $this->conexion->close();
        }
        function ultimo_id(){
            return $this->conexion->insert_id;
        }
        function lanzar_consulta($sql, $parametros= array()){
            $sentencia=$this->conexion->prepare($sql);

            if(!empty($parametros)){
                $tipos="";
                $palabra="string";
                $numero="int";
                foreach ($parametros as $parametro){
                    if(gettype($parametro==$palabra)){
                        $tipos=$tipos.'s';
                    }
                    else if(gettype($parametro==$numero)){
                        $tipos=$tipos.'i';
                    }
                }
                $sentencia->bind_param($tipos, ...$parametros);
            }
            $sentencia->execute();
            $resultado=$sentencia->get_result();
            $sentencia->close();

            return $resultado;
        }
    }
?>