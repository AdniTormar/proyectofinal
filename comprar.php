<?php
include("conf/configuracion.php");
include ("funcion/funcion.php");

$db=new Db();
$db->conectar();

$sqlDT="select now()";
$resultadoDT=$db->lanzar_consulta($sqlDT);
$filaDT=$resultadoDT->fetch_row();
$fecha_compra = date("Y-m-d H:i:s", strtotime($filaDT[0]));

$foto_ver=$_GET["foto_ver"];
$juego=$_GET["juego"];
$id_cesta=$_GET["id_cesta"];
$id_usuario=$_GET["id_usuario"];
$id_juego=$_GET["id_juego"];

$sqlC_K="Select codigo,id_juego from codigo_keys where id_juego=?";
$resultadoC_K = $db->lanzar_consulta($sqlC_K, array($id_juego));
while($filaC_K=$resultadoC_K->fetch_assoc()){
    //var_dump($filaC_K);
    if($filaC_K["id_juego"]==$id_juego){
        $coger_codigo=$filaC_K["codigo"];
        break;
    }
    else{

    }
}
if(isset($coger_codigo)) {
    $sqlCC = "insert into juegos_comprados (foto_ver,fecha_compra,codigo,juego) VALUES (?,?,?,?)";
    $resultadoCC = $db->lanzar_consulta($sqlCC, array($foto_ver, $fecha_compra, $coger_codigo, $juego));

    $sql = ("select COUNT(*) as 'cantidad' ,codigo as 'codigo',juegos_comprados.id as 'id' ,juego as 'juego' from juegos_comprados,juegos where juegos.titulo=juegos_comprados.juego and juegos_comprados.juego=? and juegos_comprados.codigo=?");

    $resultado = $db->lanzar_consulta($sql, array($juego,$coger_codigo));
    $fila = $resultado->fetch_assoc();
    if($fila["cantidad"]>1){
        header("location: cesta.php");
    }
    else{
        $nopasar=1;
        var_dump($nopasar);
    }
    if($nopasar==1){
        if ($juego == $fila["juego"]) {
            $sqlUJC = "insert into usuarios_juegos_comprados (id_usuario, id_juegos_comprados) VALUES (?,?)";
            $resultadoUJC = $db->lanzar_consulta($sqlUJC, array($id_usuario, $fila["id"]));

            $sqlCK= "DELETE FROM codigo_keys WHERE codigo=?";
            $resultadoCK = $db->lanzar_consulta($sqlCK, array($coger_codigo));

            $sqlJ_C = "DELETE FROM juegos_cesta WHERE id_cesta = ?";
            $resultadoJ_C = $db->lanzar_consulta($sqlJ_C, array($id_cesta));

            $sqlC = "DELETE FROM cesta WHERE juego=? and id=?";
            $resultadoC = $db->lanzar_consulta($sqlC, array($juego, $id_cesta));
            header("location: perfil.php?juegos_comprados=1");
        }
    }
}
$db->desconectar();
?>