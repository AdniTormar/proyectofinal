<?php
if(isset($_SESSION["id"])){
    if(isset($_SESSION["Admin"])){

    }
    else{
        header("location: index.php");
    }
}
if(isset($_SESSION["idCo"])){
    if(isset($_SESSION["AdminCo"])){

    }
    else{
        header("location: index.php");
    }
}
?>
<?php

$sql = "SELECT apodo, usuario,nombre,apellidos, direccion, telefono, correo, tipo_usuario,baneado,id FROM usuarios ";
$resultado=$db->lanzar_consulta($sql);
while($fila=$resultado->fetch_assoc()){
    ?>
    <tr>
        <td><?= $fila["apodo"] ?></td>
        <td><?= $fila["usuario"] ?></td>
        <td><?= "********" ?></td>
        <td><?= $fila["nombre"] ?></td>
        <td><?= $fila["apellidos"] ?></td>
        <td><?= $fila["direccion"] ?></td>
        <td><?= $fila["telefono"] ?></td>
        <td><?= $fila["correo"] ?></td>
        <td><?= $fila["tipo_usuario"] ?></td>
        <td><?= $fila["baneado"] ?></td>
        <td><button><a href="borrar.php?id_usuario=<?= $fila['id']?>">Eliminar</a></button></td>
        <input type="hidden" name="id"  value="actualizar_usuario_control">
    </tr>
    <?php
}
?>
<button><a data-toggle="modal" data-target="#miModal" href="">Modificar Usuario</a></button>
<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog vmodal2" role="document">
        <div class="modal-content ventanamodal2">
            <div class="modal-body">
                <form class="form-signin" method="post" action="actualizar_usuario_control.php" enctype="multipart/form-data">
                    <input class="form-control" type="text" id="apodo" name="apodo" class="form-control" placeholder="Cambiar Apodo" maxlength="7" required autofocus>
                    <br>
                    <input class="form-control" type="text" id="usuario" name="usuario" class="form-control" placeholder="Cambiar Usuario" maxlength="6" minlength="5" required autofocus>
                    <br>
                    <input class="form-control" type="password" id="contrasena" name="contrasena" class="form-control" placeholder="Cambiar Contraseña" maxlength="15" minlength="8" required autofocus>
                    <br>
                    <input class="form-control" type="text" id="nombre" name="nombre" class="form-control" placeholder="Cambiar Nombre" required autofocus>
                    <br>
                    <input class="form-control" type="text" id="apellidos" name="apellidos" class="form-control" placeholder="Cambiar Apellidos" required autofocus>
                    <br>
                    <input class="form-control" type="text" id="direccion" name="direccion" class="form-control" placeholder="Cambiar Dirección" required autofocus>
                    <br>
                    <input class="form-control" type="text" id="telefono" name="telefono" class="form-control" placeholder="Cambiar Telefono" required autofocus maxlength="9" minlength="9">
                    <br>
                    <input class="form-control" type="text" id="correo" name="correo" class="form-control" placeholder="Cambiar Correo" required autofocus>
                    <br>
                    <select class="form-control" id="tipo_usuario" name="tipo_usuario" required autofocus>
                        <option value="" selected>Cambiar Tipo Usuario</option>
                        <option value="1">Administrador
                        <option value="2">Editor
                        <option value="3">Usuario
                    </select>
                    <br>
                    <select class="form-control" id="baneado" name="baneado" required autofocus>
                        <option value="" selected>Cambiar Baneado</option>
                        <option value="1">Si
                        <option value="0">No
                    </select>
                    <br>
                    <select class="form-control" id="id_usuario" name="id_usuario" required autofocus>
                        <option value="" selected>Seleccionar Usuario</option>
                        <?php
                        $sqlUsuario="select id, usuario from usuarios";
                        $resultadoUsuario=$db->lanzar_consulta($sqlUsuario);
                        while($filaUsuario = $resultadoUsuario->fetch_assoc()){
                            ?>
                            <option value="<?=$filaUsuario["id"] ?>"><?=$filaUsuario["usuario"]?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <br>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>