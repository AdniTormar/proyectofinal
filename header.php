<?php
if(isset($_SESSION["baneado"])){
    if($_SESSION["baneado"]==1){
        ?>
        <?php
        session_start();
        session_unset();
        session_destroy();
        $mensaje="Has sido baneado durante un tiempo indefinido por comentarios ostiles o otro tipo de comentarios. Para contactar con nosotros: jog@gmail.com";
        $mensaje= base64_encode($mensaje);
        header("location:index.php?mensaje=".$mensaje);
    }
    else{

    }
}
else{

}
?>
<ul class="navbar-nav mr-auto">
    <?php
    if(!isset($_SESSION['Usuario'])&&!isset($_SESSION['Admin'])&&!isset($_SESSION['Editor'])&&!isset($_SESSION['UsuarioCo'])&&!isset($_SESSION['AdminCo'])&&!isset($_SESSION['EditorCo'])){
        ?>
        <li class="nav-item">
            <a class="nav-link active" href="index.php">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="list_games.php">Juegos <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="new_account.php">Crear Cuenta <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="login.php">Inicio Sesion <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="comentarios.php">Comentarios</a>
        </li>
        <?php
        if(isset($estaenjuego)==true){
        ?>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Busqueda
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <form class="form-inline my-2 my-lg-0">
                    <input class="dropdown-item" name= "cajabusqueda" type="text" action="busqueda.php" method="get" placeholder="Por Titulo" aria-label="Search" required autofocus>
                    <input class="dropdown-item" name= "cajabusqueda2" type="text" action="busqueda.php" method="get" placeholder="Por Genero" aria-label="Search">
                    <select class="dropdown-item" name= "cajabusqueda3" action="busqueda.php" method="get" placeholder="Plataforma" aria-label="Search">
                        <option selected disabled value="0">Plataforma
                        <option value="Steam">Steam
                        <option value="Origin">Origin
                        <option value="Uplay">Uplay
                        <option value="Battle.net">Battle.net
                        <option value="PS4">PS4
                        <option value="Nintendo">Nintendo
                    </select>
                    <div class="dropdown-item">DLC<input name= "cajabusqueda4" type="checkbox" action="busqueda.php" method="get" placeholder="Dlc" aria-label="Search"></div>
                    <input type="hidden" name="id"  value="busqueda">
                    <div class="dropdown-divider"></div>
                    <button class="dropdown-item" type="submit"><a id="busqueda" class="ColorC">Buscar</a></button>
                </form>
            </div>
        </li>
        <?php
        }
    }
    ?>
    <?php
    if(isset($_SESSION["idCo"])){
        ?>
        <?php
        if(isset($_SESSION['UsuarioCo'])||isset($_SESSION['AdminCo'])||isset($_SESSION['EditorCo'])) {
            ?>
            <li class="nav-item">
                <a class="nav-link active usuarioBajar" href="index.php">Inicio <span
                            class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link active usuarioBajar" href="list_games.php">Juegos <span
                            class="sr-only">(current)</span></a>
            </li>
            <?php
            if (isset($_SESSION['AdminCo'])||isset($_SESSION["EditorCo"])){
            ?>
            <li class="nav-item">
                <a class="nav-link active usuarioBajar" href="new_game.php">Creación Games <span class="sr-only">(current)</span></a>
            </li>
            <?php
             }
            ?>
            <li class="nav-item">
                <a class="nav-link active usuarioBajar" href="new_account.php">Crear Cuenta <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link usuarioBajar" href="perfil.php?info=1">Perfil</a>
            </li>
            <?php
        }
        ?>
        <?php
        if(isset($_SESSION['UsuarioCo'])||isset($_SESSION['EditorCo'])||isset($_SESSION['AdminCo'])) {
            ?>
            <?php
            $sql2="select count(*) from cesta c, usuarios u where u.id=c.id_usuario and c.id_usuario=?";
            if(isset($_SESSION["id"])){
                $resultado2=$db->lanzar_consulta($sql2,array($_SESSION['id']));
            }
            if(isset($_SESSION["idCo"])){
                $resultado2=$db->lanzar_consulta($sql2,array($_SESSION['idCo']));
            }
            $fila2=$resultado2->fetch_row();
            ?>
            <li class="nav-item">
                <a class="nav-link usuarioBajar" id="cj" href="<?php if($fila2[0]>0){?>cesta.php<?php } ?>"><img src="img/cesta.png" style="width: 10px;height: 10px">Cesta: <?=$fila2[0] ?></a>
            </li>
            <?php
        }
        ?>
        <li class="nav-item">
            <a class="nav-link active usuarioBajar" href="logout.php">Cerrar Sesion <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled usuarioBajar" href="comentarios.php">Comentarios</a>
        </li>
        <?php
        if(isset($estaenjuego)==true){
            ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle usuarioBajar" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Busqueda
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <form class="form-inline my-2 my-lg-0">
                        <input class="dropdown-item" name= "cajabusqueda" type="text" action="busqueda.php" method="get" placeholder="Por Titulo" aria-label="Search" required autofocus>
                        <input class="dropdown-item" name= "cajabusqueda2" type="text" action="busqueda.php" method="get" placeholder="Por Genero" aria-label="Search">
                        <select class="dropdown-item" name= "cajabusqueda3" action="busqueda.php" method="get" placeholder="Plataforma" aria-label="Search">
                            <option selected disabled value="0">Plataforma
                            <option value="Steam">Steam
                            <option value="Origin">Origin
                            <option value="Uplay">Uplay
                            <option value="Battle.net">Battle.net
                            <option value="PS4">PS4
                            <option value="Nintendo">Nintendo
                        </select>
                        <div class="dropdown-item">DLC<input name= "cajabusqueda4" type="checkbox" action="busqueda.php" method="get" placeholder="Dlc" aria-label="Search"></div>
                        <input type="hidden" name="id"  value="busqueda">
                        <div class="dropdown-divider"></div>
                        <button class="dropdown-item" type="submit"><a id="busqueda" class="ColorC">Buscar</a></button>
                    </form>
                </div>
            </li>
            <?php
        }
        ?>
        <?php
        if(isset($_SESSION["UsuarioCo"])||isset($_SESSION["AdminCo"])||isset($_SESSION["EditorCo"])){
            ?>
            <li class="nav-item">
                <a href="perfil.php?info=1" class="nav-link <?php if(isset($_SESSION["UsuarioCo"])){?>usuarioCo<?php } ?><?php if(isset($_SESSION["AdminCo"])){?>usuarioCoAdmin<?php } ?><?php if(isset($_SESSION["EditorCo"])){?>usuarioCoEditor<?php } ?>" href=""><img class="usuario" src="img/<?php if(isset($_SESSION["UsuarioCo"]))echo $_SESSION["imagen"] ?><?php if(isset($_SESSION["AdminCo"]))echo $_SESSION["imagen"] ?><?php if(isset($_SESSION["EditorCo"]))echo $_SESSION["imagen"] ?>">  <?php if(isset($_SESSION["UsuarioCo"]))echo $_SESSION["UsuarioCo"]?><?php if(isset($_SESSION["EditorCo"]))echo $_SESSION["EditorCo"]?><?php if(isset($_SESSION["AdminCo"]))echo $_SESSION["AdminCo"]?> <span class="sr-only">(current)</span></a>
            </li>
            <?php
        }
        ?>
        <?php
    }
    ?>
    <?php
    if(isset($_SESSION["id"])){
        ?>
        <?php
        if(isset($_SESSION['Usuario'])||isset($_SESSION['Admin'])||isset($_SESSION['Editor'])){
            ?>
            <li class="nav-item">
                <a class="nav-link active usuarioBajar" href="index.php">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link active usuarioBajar" href="list_games.php">Juegos <span
                            class="sr-only">(current)</span></a>
            </li>
            <?php
            if (isset($_SESSION['Admin'])||isset($_SESSION["Editor"])){
                ?>
                <li class="nav-item">
                    <a class="nav-link active usuarioBajar" href="new_game.php">Creación Games <span class="sr-only">(current)</span></a>
                </li>
                <?php
            }
            ?>
            <li class="nav-item">
                <a class="nav-link active usuarioBajar" href="new_account.php">Crear Cuenta <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link usuarioBajar" href="perfil.php?info=1">Perfil</a>
            </li>
            <?php
        }
        ?>
        <?php
        if(isset($_SESSION['Usuario'])||isset($_SESSION['Editor'])||isset($_SESSION['Admin'])) {
            ?>
            <?php
            $sql2="select count(*) from cesta c, usuarios u where u.id=c.id_usuario and c.id_usuario=?";
            if(isset($_SESSION["id"])){
                $resultado2=$db->lanzar_consulta($sql2,array($_SESSION['id']));
            }
            if(isset($_SESSION["idCo"])){
                $resultado2=$db->lanzar_consulta($sql2,array($_SESSION['idCo']));
            }
            $fila2=$resultado2->fetch_row();
            ?>
            <li class="nav-item">
                <a class="nav-link usuarioBajar" id="cj" href="<?php if($fila2[0]>0){?>cesta.php<?php } ?>"><img src="img/cesta.png" style="width: 10px;height: 10px">Cesta: <?=$fila2[0] ?></a>
            </li>
            <?php
        }
        ?>
        <li class="nav-item">
            <a class="nav-link active usuarioBajar" href="logout.php">Cerrar Sesion <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled usuarioBajar" href="comentarios.php">Comentarios</a>
        </li>
        <?php
        if(isset($estaenjuego)==true){
            ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle usuarioBajar" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Busqueda
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <form class="form-inline my-2 my-lg-0">
                        <input class="dropdown-item" name= "cajabusqueda" type="text" action="busqueda.php" method="get" placeholder="Por Titulo" aria-label="Search" required autofocus>
                        <input class="dropdown-item" name= "cajabusqueda2" type="text" action="busqueda.php" method="get" placeholder="Por Genero" aria-label="Search">
                        <select class="dropdown-item" name= "cajabusqueda3" action="busqueda.php" method="get" placeholder="Plataforma" aria-label="Search">
                            <option selected disabled value="0">Plataforma
                            <option value="Steam">Steam
                            <option value="Origin">Origin
                            <option value="Uplay">Uplay
                            <option value="Battle.net">Battle.net
                            <option value="PS4">PS4
                            <option value="Nintendo">Nintendo
                        </select>
                        <div class="dropdown-item">DLC<input name= "cajabusqueda4" type="checkbox" action="busqueda.php" method="get" placeholder="Dlc" aria-label="Search"></div>
                        <input type="hidden" name="id"  value="busqueda">
                        <div class="dropdown-divider"></div>
                        <button class="dropdown-item" type="submit"><a id="busqueda" class="ColorC">Buscar</a></button>
                    </form>
                </div>
            </li>
            <?php
        }
        ?>
        <?php
        if(isset($_SESSION["Usuario"])||isset($_SESSION["Admin"])||isset($_SESSION["Editor"])){
            ?>
            <li class="nav-item">
                <a href="perfil.php?info=1" class="nav-link <?php if($estaenjuego==true){ ?><?php if(isset($_SESSION["Usuario"])){?>usuario2<?php } ?><?php if(isset($_SESSION["Admin"])){?>usuarioAdmin2<?php } ?><?php if(isset($_SESSION["Editor"])){?>usuarioEditor2<?php }} ?> <?php if($estaenjuego==false){ ?><?php if(isset($_SESSION["Usuario"])){?>usuario<?php } ?><?php if(isset($_SESSION["Admin"])){?>usuarioAdmin<?php } ?><?php if(isset($_SESSION["Editor"])){?>usuarioEditor<?php }} ?>" href=""><img class="usuario" src="img/<?php if(isset($_SESSION["Usuario"]))echo $_SESSION["imagen"] ?><?php if(isset($_SESSION["Admin"]))echo $_SESSION["imagen"] ?><?php if(isset($_SESSION["Editor"]))echo $_SESSION["imagen"] ?>">  <?php if(isset($_SESSION["Usuario"]))echo $_SESSION["Usuario"]?><?php if(isset($_SESSION["Editor"]))echo $_SESSION["Editor"]?><?php if(isset($_SESSION["Admin"]))echo $_SESSION["Admin"]?> <span class="sr-only">(current)</span></a>
            </li>
            <?php
        }
        ?>
        <?php
    }
    ?>
</ul>