<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();
if(isset($_GET["genero"])){
    $genero="%".$_GET["genero"]."%";
}
else{

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
</head>
<body>
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
        include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>
<div style="width:100px;position:absolute;left:25px;">
    <table cellspacing="3" cellpadding="3">
        <tr>
            <?php
            define("TAMANO_PAGINA", 12);
            $contador=0;
            if(isset($_REQUEST["pagina"])){
                $pagina=$_REQUEST["pagina"];
            }
            else{
                $pagina = 0;
            }

            $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and genero LIKE ?";
            $resultado3=$db->lanzar_consulta($sql3,array(1,$genero));
            $fila3=$resultado3->fetch_assoc();
            $entradas=$fila3["cantidad"];
            $paginas=$entradas / TAMANO_PAGINA;

            $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver,id,disponible,plataforma,dlc FROM juegos where disponible=? and genero LIKE ? LIMIT " . $pagina  * TAMANO_PAGINA . ", " . TAMANO_PAGINA;
            $resultado=$db->lanzar_consulta($sql,array(1,$genero));
            while($fila=$resultado->fetch_assoc()){
                ?>
                <?php
                $contador++;
                ?>
                <?php
                if($contador==13) {
                    $contador=0;
                }
                if($contador<=4) {
                    ?>
                    <td>
                        <div>
                            <div class="card juego">
                                <img class="card-img-top ver" src="img/<?= $fila['foto_ver'] ?>" alt="Card image cap">
                                <?php
                                if($fila["plataforma"]=="steam"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/steam.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="origin"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/origin.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="uplay"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/uplay.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="battle.net"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/battle.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="ps4"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/ps4.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="nintendo"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/nintendo.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["dlc"]==1){
                                    ?>
                                    <img class="card-img-top ver3" src="img/dlc.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <div class="card-body">
                                    <h4 class="card-title"><?= $fila["titulo"] ?></h4>
                                    <p class="card-text"><?= $fila['descripcion'] ?>...</p>
                                    <a href="list_games.php?id=game&id_juego=<?= $fila["id"] ?><?= $fila["titulo"] ?>" class="btn btn-primary">Leer
                                        más</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php
                }
                ?>
                <?php
                if($contador>=5) {
                    ?>
                    <td>
                        <div>
                            <div class="card <?php if($contador==5){?>juego2<?php } ?><?php if($contador==6){?>juego3<?php } ?><?php if($contador==7){?>juego4<?php } ?><?php if($contador==8){?>juego5<?php } ?><?php if($contador==9){?>juego6<?php } ?><?php if($contador==10){?>juego7<?php } ?><?php if($contador==11){?>juego8<?php } ?><?php if($contador==12){?>juego9<?php } ?>">
                                <img class="card-img-top ver" src="img/<?= $fila['foto_ver'] ?>" alt="Card image cap">
                                <?php
                                if($fila["plataforma"]=="steam"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/steam.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="origin"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/origin.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="uplay"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/uplay.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="battle.net"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/battle.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="ps4"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/ps4.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["plataforma"]=="nintendo"){
                                    ?>
                                    <img class="card-img-top ver2" src="img/nintendo.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <?php
                                if($fila["dlc"]==1){
                                    ?>
                                    <img class="card-img-top ver3" src="img/dlc.png" alt="Card image cap">
                                    <?php
                                }
                                ?>
                                <div class="card-body">
                                    <h4 class="card-title"><?= $fila["titulo"] ?></h4>
                                    <p class="card-text"><?= $fila['descripcion'] ?>...</p>
                                    <a href="list_games.php?id=game&id_juego=<?= $fila["id"] ?><?= $fila["titulo"] ?>"
                                       class="btn btn-primary">Leer
                                        más</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php
                }
            }
            ?>
            <nav class="<?php if($contador<=4 && $contador<5){?>paginacion3<?php } ?><?php if($contador>=5 && $contador<9){?>paginacion2<?php } ?><?php if($contador>=9){?>paginacion<?php } ?>" aria-label="Page navigation example">
                <ul class="pagination">
                    <?php
                    $comprobar=explode("%",$genero);
                    $genero=$comprobar[1];
                    for ($i = 0;$i<$paginas;$i++){
                        ?>
                        <li class="page-item"><a class="page-link" href="?genero=<?php echo $genero;?>&pagina=<?=$i ?>" > <?=$i + 1?> </a></li>
                        <?php
                    }
                    ?>
                    <li class="page-item"><a class="page-link" > ... </a></li>
                </ul>
            </nav>
        </tr>
    </table>
</div>
<!-- Abajo -->
<div class="container">
    <div align="center" class="<?php if($contador<=4 && $contador<5){?>abajo9<?php } ?><?php if($contador>=5 && $contador<9){?>abajo7<?php } ?><?php if($contador>=9){?>abajo8<?php } ?>">
        <?php
            include("pie.php");
        ?>
    </div>
</div>
<?php
$db->desconectar();
?>
</body>
</html>
