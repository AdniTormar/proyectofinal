<?php
include("conf/configuracion.php");
include ("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();
if(isset($_SESSION["id"])){
    if(isset($_SESSION["Admin"])){
        $usuario=1;
        if(isset($_GET["juegos"])){
            $usuario=0;
            $juegos=1;
        }
    }
    else{
        header("location: index.php");
    }
}
if(isset($_SESSION["idCo"])){
    if(isset($_SESSION["AdminCo"])){
        $usuario=1;
        if(isset($_GET["juegos"])){
            $usuario=0;
            $juegos=1;
        }
    }
    else{
        header("location: index.php");
    }
}
if(isset($_REQUEST["mensaje"]))
    $mensaje=$_REQUEST["mensaje"];
else
    $mensaje="";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/gif" href="img/icono.png" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
    <SCRIPT src="libs/ckeditor/ckeditor.js"></SCRIPT>

    <title>Dashboard Template for Bootstrap</title>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
</head>

<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="panel_control.php">Panel Admin</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="new_game.php">Crear Juego</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="new_account.php">Crear Cuenta</a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<div class="container-fluid">
    <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link <?php if($usuario==1){ ?>active<?php } ?>" href="panel_control.php">Usuarios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($juegos==1){ ?>active<?php } ?>" href="panel_control.php?juegos=juegos">Ver juegos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
            </ul>

            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
            </ul>

            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                </li>
            </ul>
        </nav>

        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
            <h1>Panel de Control Admin</h1>
            <a class="btn btn-warning" href="<?php if(isset($usuario)&&$usuario==1) { ?>informe_entrada.php <?php } ?><?php if(isset($juegos)&&$juegos==1) { ?>informe_entrada2.php <?php } ?>">Informe</a>
            <?php
            if(isset($usuario)&&$usuario==1) {
                ?>
                <h2>Usuarios</h2>
                <?php
            }
            ?>
            <?php
            if(isset($juegos)&&$juegos==1) {
                ?>
                <h2>Juegos</h2>
                <?php
            }
            ?>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <?php
                        if(isset($usuario)&&$usuario==1) {
                            ?>
                            <th>Apodo</th>
                            <th>Usuario</th>
                            <th>Contraseña</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Dirección</th>
                            <th>Telefono</th>
                            <th>Correo</th>
                            <th>Perfil</th>
                            <th>Baneado</th>
                            <?php
                        }
                        ?>
                        <?php
                        if(isset($juegos)&&$juegos==1){
                            ?>
                            <th>Titulo</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Genero</th>
                            <th>Empresa</th>
                            <th>Edad</th>
                            <th>Fecha</th>
                            <th>Foto Juego</th>
                            <th>Foto Fondo</th>
                            <th>Trailer</th>
                            <th>Dlc</th>
                            <th>Plataforma</th>
                            <th>Disponible</th>
                        <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(isset($usuario)&&$usuario==1){
                        include("control_usuarios.php");
                        ?>
                        <?php
                        if($mensaje != ""){
                            ?>
                            <div class="alert alert-danger perfilM" align="center">
                                <?=base64_decode($mensaje) ?>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                    }
                    if(isset($juegos)&&$juegos==1){
                        include("control_juegos.php");
                        ?>
                        <?php
                        if($mensaje != ""){
                            ?>
                            <div class="alert alert-danger perfilM" align="center">
                                <?=base64_decode($mensaje) ?>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script>
</body>
</html>
<?php
$db->desconectar();
?>