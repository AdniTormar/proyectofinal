<?php
include("conf/configuracion.php");
include("funcion/funcion.php");

$db=new Db();
$db->conectar();

$usuario=$_POST['usuario'];
$contrasena=$_POST['contrasena'];
if(strpos($usuario,"@gmail.com")){
    $correo=str_replace("@","@",$usuario);
}
else if(strpos($usuario,"@hotmail.com")){
    $correo=str_replace("@","@",$usuario);
}
else{
    $correo=null;
}
if($usuario==null||$contrasena==null){
    $db->desconectar();
    header("location: index.php");
}

if(strpos($usuario, '@')){
    $sql ="Select id,baneado as 'baneado', tipo_usuario as 'perfil', imagen_usuario as 'imagen' from usuarios where contrasena = SHA1(?) and correo = ?";

    $resultado =$db->lanzar_consulta($sql, array($contrasena, $correo));
}
else{
    $sql ="Select id,baneado as 'baneado', tipo_usuario as 'perfil', imagen_usuario as 'imagen' from usuarios where usuario = ? and contrasena = SHA1(?)";

    $resultado =$db->lanzar_consulta($sql, array($usuario, $contrasena));
}

if ($resultado->num_rows == 0) {
    // No coinciden
    $db->desconectar();
    header('Location: login.php?mensaje=Usuario/Contraseña incorrectos');
    exit();
}
else{
    session_start();
    $fila = $resultado->fetch_assoc();
    if(!($correo==null)){
        if($fila["perfil"]=="Admin"){
            $sqlC=("select imagen_perfil_fondo as 'fondo' ,id_usuario as 'comprobar' from perfil");
            $resultadoC =$db->lanzar_consulta($sqlC);
            while($filaC=$resultadoC->fetch_assoc()){
                if($filaC["comprobar"]==$fila["id"]){
                    $_SESSION["fondo"]=$filaC["fondo"];
                }
            }
            $_SESSION["AdminCo"]=$correo;
            //$_SESSION["baneado"] = $fila["baneado"];
            $_SESSION["idCo"] = $fila["id"];
            $_SESSION["imagen"] = $fila["imagen"];
            $db->desconectar();
        }
        else if($fila["perfil"]=="Editor"){
            $sqlC=("select imagen_perfil_fondo as 'fondo' ,id_usuario as 'comprobar' from perfil");
            $resultadoC =$db->lanzar_consulta($sqlC);
            while($filaC=$resultadoC->fetch_assoc()){
                if($filaC["comprobar"]==$fila["id"]){
                    $_SESSION["fondo"]=$filaC["fondo"];
                }
            }
            $_SESSION["EditorCo"]=$correo;
            //$_SESSION["baneado"] = $fila["baneado"];
            $_SESSION["idCo"] = $fila["id"];
            $_SESSION["imagen"] = $fila["imagen"];
            $db->desconectar();
        }
        else if($fila["perfil"]=="Usuario"){
            $sqlC=("select imagen_perfil_fondo as 'fondo' ,id_usuario as 'comprobar' from perfil");
            $resultadoC =$db->lanzar_consulta($sqlC);
            while($filaC=$resultadoC->fetch_assoc()){
                if($filaC["comprobar"]==$fila["id"]){
                    $_SESSION["fondo"]=$filaC["fondo"];
                }
            }
            $_SESSION["UsuarioCo"]=$correo;
            //$_SESSION["baneado"] = $fila["baneado"];
            $_SESSION["idCo"] = $fila["id"];
            $_SESSION["imagen"] = $fila["imagen"];
            $db->desconectar();
        }
        header('location:index.php');
    }
    else{
        if($fila["perfil"]=="Admin"){
            $sqlC=("select imagen_perfil_fondo as 'fondo' ,id_usuario as 'comprobar' from perfil");
            $resultadoC =$db->lanzar_consulta($sqlC);
            while($filaC=$resultadoC->fetch_assoc()){
                if($filaC["comprobar"]==$fila["id"]){
                    $_SESSION["fondo"]=$filaC["fondo"];
                }
            }
            $_SESSION["Admin"]=$usuario;
            //$_SESSION["baneado"] = $fila["baneado"];
            $_SESSION["id"] = $fila["id"];
            $_SESSION["imagen"] = $fila["imagen"];
            $db->desconectar();
        }
        else if($fila["perfil"]=="Editor"){
            $sqlC=("select imagen_perfil_fondo as 'fondo' ,id_usuario as 'comprobar' from perfil");
            $resultadoC =$db->lanzar_consulta($sqlC);
            while($filaC=$resultadoC->fetch_assoc()){
                if($filaC["comprobar"]==$fila["id"]){
                    $_SESSION["fondo"]=$filaC["fondo"];
                }
            }
            $_SESSION["Editor"]=$usuario;
            //$_SESSION["baneado"] = $fila["baneado"];
            $_SESSION["id"] = $fila["id"];
            $_SESSION["imagen"] = $fila["imagen"];
            $db->desconectar();
        }
        else if($fila["perfil"]=="Usuario"){
            $sqlC=("select imagen_perfil_fondo as 'fondo' ,id_usuario as 'comprobar' from perfil");
            $resultadoC =$db->lanzar_consulta($sqlC);
            while($filaC=$resultadoC->fetch_assoc()){
                if($filaC["comprobar"]==$fila["id"]){
                    $_SESSION["fondo"]=$filaC["fondo"];
                }
            }
            $_SESSION["Usuario"]=$usuario;
            $_SESSION["baneado"] = $fila["baneado"];
            //var_dump($_SESSION["baneado"]);
            //exit();
            $_SESSION["id"] = $fila["id"];
            $_SESSION["imagen"] = $fila["imagen"];
            $db->desconectar();
        }
        header('location:index.php');
    }
}
?>