<?php
include("conf/configuracion.php");
include("funcion/funcion.php");

$db=new Db();
$db->conectar();

$apodo = $_POST["apodo"];
$usuario = $_POST["usuario"];
$contrasena = $_POST["contrasena"];
$contrasena2 = $_POST["contrasena2"];
$nombre = $_POST["nombre"];
$apellidos = $_POST["apellidos"];
$direccion = $_POST["direccion"];
$telefono = $_POST["telefono"];
$correo = $_POST["correo"];
$perfil = $_POST["perfil"];
$baneado = 0;
$imagen = $_FILES["imagen"]["name"];
$tmp_imagen = $_FILES["imagen"]["tmp_name"];
$imagenP = $_FILES["imagenP"]["name"];
$tmp_imagenP = $_FILES["imagenP"]["tmp_name"];

$ruta_imagen="img/".$imagen;
move_uploaded_file($tmp_imagen, $ruta_imagen);


$ruta_imagenP="img/imgperfil/".$imagenP;
move_uploaded_file($tmp_imagenP, $ruta_imagenP);


/*var_dump($apodo);
var_dump($usuario);
var_dump($contrasena);
var_dump($contrasena2);
var_dump($nombre);
var_dump($apellidos);
var_dump($direccion);
var_dump($telefono);
var_dump($correo);
var_dump($perfil);
var_dump($baneado);
var_dump($imagen);
var_dump($tmp_imagen);
var_dump($ruta_imagen);
var_dump($imagenP);
var_dump($tmp_imagenP);
var_dump($ruta_imagenP);
exit();*/
if($apodo==null ||$usuario==null || $imagen==null || $imagenP==null || $nombre==null|| $apellidos==null || $contrasena==null || $direccion==null || $telefono==null || $correo==null || $perfil ==null ){
    $db->desconectar();
    $mensaje="Error 15";
    $mensaje= base64_encode($mensaje);
    header("location: new_account.php?mensaje=".$mensaje);
}

$sql2=("select COUNT(*)AS 'cantidad',usuario as 'usu',apodo as 'apo',telefono as 'tele',correo as 'co' from usuarios where apodo=? or usuario=? or telefono=? or correo=?");

$resultado2 =$db->lanzar_consulta($sql2, array($apodo,$usuario,$telefono,$correo));

$fila2=$resultado2->fetch_assoc();

if(!($contrasena==$contrasena2)){
    $db->desconectar();
    $mensaje="Contraseña mál puesta";
    $mensaje= base64_encode($mensaje);
    header("location: new_account.php?mensaje=".$mensaje);
}
else if($fila2['cantidad']>=1) {
    if ($usuario == $fila2['usu']) {
        $db->desconectar();
        $mensaje="Usuario Existente";
        $mensaje= base64_encode($mensaje);
        header("location: new_account.php?mensaje=".$mensaje);
    }

    if ($apodo == $fila2['apo']) {
        $db->desconectar();
        $mensaje="Apodo Existente";
        $mensaje= base64_encode($mensaje);
        header("location: new_account.php?mensaje=".$mensaje);
    }
    if ($telefono == $fila2['tele']) {
        $db->desconectar();
        $mensaje="Telefono Existente";
        $mensaje= base64_encode($mensaje);
        header("location: new_account.php?mensaje=".$mensaje);
    }
    if ($correo == $fila2['co']) {
        $db->desconectar();
        $mensaje="Correo Existente";
        $mensaje= base64_encode($mensaje);
        header("location: new_account.php?mensaje=".$mensaje);
    }
}
else{

    $sql="insert into usuarios (apodo,imagen_usuario,usuario,contrasena,nombre,apellidos,direccion,telefono,correo,tipo_usuario,baneado) VALUES (?,?,?,SHA1(?),?,?,?,?,?,?,?)";
    $resultado=$db->lanzar_consulta($sql, array($apodo,$imagen,$usuario,$contrasena,$nombre,$apellidos,$direccion,$telefono,$correo,$perfil,$baneado));

    $sqlc=("select id as 'id', usuario as 'comprobar' from usuarios where usuario=?");

    $resultadoc =$db->lanzar_consulta($sqlc, array($usuario));

    $filac=$resultadoc->fetch_assoc();
    if($usuario==$filac["comprobar"]){
        $sql3="insert into perfil (imagen_perfil_fondo,id_usuario) VALUES (?,?)";
        $resultado3=$db->lanzar_consulta($sql3, array($imagenP,$filac["id"]));
    }
    $db->desconectar();
    $mensaje="Cuenta Creada";
    $mensaje= base64_encode($mensaje);
    header("location: new_account.php?mensaje=".$mensaje);
}
?>