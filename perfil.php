<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();
if(isset($_GET["info"])==0){
    $info=0;
    $juegos_comprado=1;
}
else{
    $info=1;
    $juegos_comprado=0;
    $sql="select id,apodo,usuario,nombre,apellidos,telefono,direccion,correo from usuarios where id=?";
    if(isset($_SESSION["id"])){
        $resultado=$db->lanzar_consulta($sql,array($_SESSION["id"]));
    }
    if(isset($_SESSION["idCo"])){
        $resultado=$db->lanzar_consulta($sql,array($_SESSION["idCo"]));
    }
    $fila=$resultado->fetch_assoc();
    if(isset($_REQUEST["mensaje"]))
        $mensaje=$_REQUEST["mensaje"];
    else
        $mensaje="";
}
if(isset($_GET["juegos_comprados"])==0){
    $info=1;
    $juegos_comprado=0;
}
else{
    $sqlC="select COUNT(*) as 'cantidad' from juegos,cesta,juegos_cesta where juegos_cesta.id_juego=juegos.id and juegos_cesta.id_cesta=cesta.id and disponible=? and cesta.id_usuario=?";
    if(isset($_SESSION["id"])){
        $resultadoC=$db->lanzar_consulta($sqlC,array(1,$_SESSION["id"]));
    }
    if(isset($_SESSION["idCo"])){
        $resultadoC=$db->lanzar_consulta($sqlC,array(1,$_SESSION["idCo"]));
    }
    $filaC=$resultadoC->fetch_assoc();
    if($filaC["cantidad"]<=0 && $filaC["cantidad"]<1){
        $falso=true;
    }
    else if($filaC["cantidad"]>0){
        $falso=false;
    }
    $sqlCodigo="select count(*) as 'cantidad' from juegos_comprados,usuarios_juegos_comprados,usuarios where juegos_comprados.id=usuarios_juegos_comprados.id_juegos_comprados and usuarios.id=usuarios_juegos_comprados.id_usuario and usuarios.id=?";
    if(isset($_SESSION["id"])){
        $resultadoCodigo=$db->lanzar_consulta($sqlCodigo,array($_SESSION["id"]));
    }
    if(isset($_SESSION["idCo"])){
        $resultadoCodigo=$db->lanzar_consulta($sqlCodigo,array($_SESSION["idCo"]));
    }
    $filaCodigo=$resultadoCodigo->fetch_assoc();
    //var_dump("hola".$filaCodigo["cantidad"]);
    if($filaCodigo["cantidad"]>0){
        $falso=false;
    }
    else if($filaCodigo["cantidad"]<=0){
        $falso=true;
    }
    $info=0;
    $juegos_comprado=1;
}
if (isset($_REQUEST["id"]))
    $id = $_REQUEST["id"];
else if(!(isset($_REQUEST["id"]))){
    $id = "view_juegos_comprados";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
</head>
<body>
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
        include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>
<img class="modificacionImgPerfil" src="img/imgperfil/<?=$_SESSION["fondo"]?>">
<div class="jumbotron Jcentrar">
    <?php
    if($info==1) {
        ?>
        <h1 align="center">Información Cuenta</h1>
        <?php
    }
    ?>
    <?php
    if($juegos_comprado==1) {
        ?>
        <h1 align="center">Lista de Juegos Comprados</h1>
        <?php
    }
    ?>
    <hr>
    <ul class="navbar-nav mr-auto <?php if($info==1){ ?>perfilHe<?php }?> <?php if($juegos_comprado==1){ ?>perfilHe3<?php }?>">
        <li class="nav-item perfil">
            <a class="nav-link active" href="?info=1">Información <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item perfil2">
            <a class="nav-link active" href="?juegos_comprados=1">Juegos Comprados <span class="sr-only">(current)</span></a>
        </li>
    </ul>
    <?php
    if($info==1){
    ?>
        <br>
        <div class="info">
            <p>Apodo: <?php echo $fila["apodo"] ?></p>
            <p>Usuario: <?php echo $fila["usuario"] ?></p>
            <p>Contraseña: <?php echo "*******" ?></p>
            <p>Nombre: <?php echo $fila["nombre"] ?></p>
            <p>Apellidos: <?php echo $fila["apellidos"] ?></p>
            <p>Telefono: <?php echo $fila["telefono"] ?></p>
            <p>Dirección: <?php echo $fila["direccion"] ?></p>
            <p>Correo: <?php echo $fila["correo"] ?></p>
            <a class="btn btn-warning mover" data-toggle="modal" data-target="#miModal" href="">Modificar</a>
            <?php
            if($mensaje != ""){
                ?>
                <div class="alert alert-danger perfilM" align="center">
                    <?=base64_decode($mensaje) ?>
                </div>
                <?php
            }
            ?>
        </div>
    <?php
    }
    ?>
    <?php
    if($juegos_comprado==1){
        ?>
        <br>
        <?php
        if($falso==true){
        ?>
        <br><br>
        <div class="alert alert-danger" align="center">
            No has comprado juegos <img class="minimizar" src="img/sorpresa.png">. Tranquilo puedes añadirlos a la cesta <a href="list_games.php">aqui</a>.
        </div>
        <?php
        }
        ?>
        <div class="div_jue2">
            <table cellspacing="3" cellpadding="3">
                <tr>
                    <?php
                    if($falso==false){
                        include ($id.".php");
                    }
                    ?>
                </tr>
            </table>
        </div>
        <?php
        if($falso==false) {
            $sqlCom = "SELECT juegos_comprados.juego,juegos_comprados.fecha_compra,juegos_comprados.foto_ver,usuarios.id as 'id_usuario',juegos.id as 'id_juego' FROM juegos_comprados,usuarios_juegos_comprados,usuarios,juegos where juegos_comprados.id=usuarios_juegos_comprados.id_juegos_comprados and usuarios.id=usuarios_juegos_comprados.id_usuario and juegos.titulo=juegos_comprados.juego and usuarios.id=? LIMIT " . $pagina * TAMANO_PAGINA . ", " . TAMANO_PAGINA;
            if (isset($_SESSION["id"])) {
                $resultadoCom = $db->lanzar_consulta($sqlCom, array($_SESSION["id"]));
            }
            if (isset($_SESSION["idCo"])) {
                $resultadoCom = $db->lanzar_consulta($sqlCom, array($_SESSION["idCo"]));
            }
            ?>
            <div class="div_jue">
                <table cellspacing="3" cellpadding="3">
                    <tr>
                        <?php
                        $contadorB = 0;
                        while ($filaCom = $resultadoCom->fetch_assoc()) {
                            $contadorB++;
                            ?>
                            <td class="<?php if ($contadorB == 1) { ?>comentar1<?php } ?><?php if ($contadorB == 2) { ?>comentar2<?php } ?><?php if ($contadorB == 3) { ?>comentar3<?php } ?><?php if ($contadorB == 4) { ?>comentar4<?php } ?><?php if ($contadorB == 5) { ?>comentar5<?php } ?><?php if ($contadorB == 6) { ?>comentar6<?php } ?><?php if ($contadorB == 7) { ?>comentar7<?php } ?><?php if ($contadorB == 8) { ?>comentar8<?php } ?><?php if ($contadorB == 9) { ?>comentar9<?php } ?><?php if ($contadorB == 10) { ?>comentar10<?php } ?><?php if ($contadorB == 11) { ?>comentar11<?php } ?><?php if ($contadorB == 12) { ?>comentar12<?php } ?>">
                                <a class="btn btn-warning"
                                   href="comentar.php?juego=<?php echo $filaCom["juego"] ?>&foto_ver=<?php echo $filaCom["foto_ver"] ?>&fecha_compra=<?php echo $filaCom["fecha_compra"] ?>&id_usuario=<?php echo $filaCom["id_usuario"] ?>&id_juego=<?php echo $filaCom["id_juego"] ?>">Comentar</a>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                </table>
            </div>
            <?php
        }
    }
    ?>
</div>
<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog vmodal2" role="document">
        <div class="modal-content ventanamodal2">
            <div class="modal-body">
                <form class="form-signin" method="post" action="actualizar_usuario.php" enctype="multipart/form-data">
                    <input type="text" id="apodo" name="apodo" class="form-control" placeholder="Apodo" maxlength="7">
                    <br>
                    <input type="file" id="imagen" name="imagen" class="form-control" placeholder="Imagen">
                    <br>
                    <input type="password" id="contrasena" name="contrasena" class="form-control" placeholder="Contraseña" maxlength="15" minlength="8">
                    <br>
                    <input type="password" id="contrasena2" name="contrasena2" class="form-control" placeholder="Confirmar Contraseña" maxlength="15" minlength="8">
                    <br>
                    <input type="text" id="direccion" name="direccion" class="form-control" placeholder="Dirección">
                    <br>
                    <input type="hidden" id="id" name="id" value="<?php echo $fila["id"] ?>">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Abajo -->
<div class="container">
    <div align="center" class="abajoPerfil">
        <?php
            include("pie.php");
        ?>
    </div>
</div>
<?php
$db->desconectar();
?>
</body>
</html>