<?php
require("libs/fpdf/fpdf.php");
require("conf/configuracion.php");
require("funcion/funcion.php");

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont("Times", 'B', 20);
$pdf->Cell(120, 10, "Informe de Juegos");
$pdf->SetFont("Times", 'B', 15);
$pdf->Cell(10, 10,"Fecha del informe: ". date("d-m-Y"));
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont("Times", '', 15);
$db = new Db();
$db->conectar();
$si="Si";
$no="No";
$contador=0;
$sql = "SELECT id, titulo,precio,genero,empresa,pegi,fecha,disponible,dlc,plataforma FROM juegos ORDER BY id DESC";
$resultado = $db->lanzar_consulta($sql);
while ($fila = $resultado->fetch_assoc()) {
    $titulo = $fila["titulo"];
    $precio = $fila["precio"];
    $genero = $fila["genero"];
    $empresa = $fila["empresa"];
    $edad = $fila["pegi"];
    $fecha = $fila["fecha"];
    $plataforma = $fila["plataforma"];
    $disponible = $fila["disponible"];
    if($disponible==1){
        $disponible=$si;
    }
    else if($disponible==0){
        $disponible=$no;
    }
    $dlc = $fila["dlc"];
    if($dlc==1){
        $dlc=$si;
    }
    else if($dlc==0){
        $dlc=$no;
    }
    $contador++;
    $pdf->Cell(40,10,$contador . " Juego");
    $pdf->Ln();
    $pdf->Cell(40, 10, "Titulo: " .  $titulo  . " | Precio: " . $precio );
    $pdf->Ln();
    $pdf->Cell(40,10, "Genero: " . $genero .  " | Empresa: " . $empresa);
    $pdf->Ln();
    $pdf->Cell(40,10, "Pegi: " . $edad . " | Disponible: " . $disponible);
    $pdf->Ln();
    $pdf->Cell(40,10, "Dlc: " . $dlc . " | Plataforma: " . $plataforma);
    $pdf->Ln();
}
$db->desconectar();

$pdf->Output();
?>