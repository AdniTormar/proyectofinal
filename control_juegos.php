<?php
if(isset($_SESSION["id"])){
    if(isset($_SESSION["Admin"])){

    }
    else{
        header("location: index.php");
    }
}
if(isset($_SESSION["idCo"])){
    if(isset($_SESSION["AdminCo"])){

    }
    else{
        header("location: index.php");
    }
}
?>
<?php

$si="Si";
$no="No";
$sql = "SELECT titulo, SUBSTR(descripcion,1,45)as'descripcion', precio, genero, empresa, pegi,fecha,dlc,plataforma,foto_ver,foto_fondo,video,disponible,id FROM juegos  ";
$resultado=$db->lanzar_consulta($sql);
while($fila=$resultado->fetch_assoc()){
    ?>
    <tr>
        <td><?= $fila["titulo"] ?></td>
        <td><?php echo strip_tags($fila["descripcion"])."...";?></td>
        <td><?= number_format($fila["precio"],2,",",".")."\xE2\x82\xAc" ?></td>
        <td><?= $fila["genero"] ?></td>
        <td><?= $fila["empresa"] ?></td>
        <td><?= $fila["pegi"] ?></td>
        <td><?= $fila["fecha"] ?></td>
        <td><?= $fila["foto_ver"] ?></td>
        <td><?= $fila["foto_fondo"] ?></td>
        <td><?= $fila["video"] ?></td>
        <td><?php if($fila["dlc"]==1){ ?><?= $si ?><?php } ?><?php if($fila["dlc"]==0){ ?><?= $no ?><?php } ?> </td>
        <td><?= $fila["plataforma"] ?></td>
        <td><?php if($fila["disponible"]==1){ ?><?= $si ?><?php } ?><?php if($fila["disponible"]==0){ ?><?= $no ?><?php } ?> </td>
        <td><button><a href="borrar.php?id_juego=<?= $fila['id']?>">Eliminar</a></button></td>
    </tr>
    <?php
}
?>
<button><a data-toggle="modal" data-target="#miModal" href="">Modificar Juego</a></button>
<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog vmodal2" role="document">
        <div class="modal-content ventanamodal2">
            <div class="modal-body">
                <form class="form-signin" method="post" action="actualizar_usuario_control_juegos.php" enctype="multipart/form-data">
                    <label for="inputEmail" class="sr-only">Titulo</label>
                    <input type="text" id="titulo" name="titulo" class="form-control" placeholder="Titulo" maxlength="30" required autofocus>
                    <br>
                    <label for="inputEmail" class="sr-only">Descripción</label>
                    <textarea name="descripcion" class="form-control" id="descripcion" placeholder="Descripcion" required autofocus></textarea>
                    <br>
                    <label for="inputPassword" class="sr-only">Precio</label>
                    <input type="text" id="precio" name="precio" class="form-control" placeholder="Precio" maxlength="6" required autofocus>
                    <br>
                    <label for="inputPassword" class="sr-only">Empresa</label>
                    <input type="text" id="empresa" name="empresa" class="form-control" placeholder="Empresa" maxlength="20" required autofocus>
                    <br>
                    <label for="inputEmail" class="sr-only">Foto Juego</label>
                    <input type="file" id="foto1" name="foto1" class="form-control" placeholder="Foto Juego" required autofocus>
                    <br>
                    <label for="inputEmail" class="sr-only">Foto Fondo</label>
                    <input type="file" id="foto2" name="foto2" class="form-control" placeholder="Foto Fondo" required autofocus>
                    <br>
                    <label for="inputEmail" class="sr-only">Video</label>
                    <input type="file" id="video" name="video" class="form-control" placeholder="Video" required autofocus>
                    <br>
                    <div class="mover2">
                        <select class="form-control mover" name="genero" id="genero" required autofocus>
                            <option value="" selected>Cambiar Genero</option>
                            <option value="1">Acción
                            <option value="2">Arcade
                            <option value="3">Aventura
                            <option value="4">Carreras
                            <option value="5">Cooperación
                            <option value="6">Multijugador
                            <option value="7">Deporte
                            <option value="8">Estrategia
                            <option value="9">FPS
                            <option value="10">Indies
                            <option value="11">Lucha
                            <option value="12">MMO
                            <option value="13">Simulación
                            <option value="14">ROL
                            <option value="15">Misterio
                            <option value="16">RPG
                            <option value="17">Mundo Abierto
                        </select>
                    </div>
                    <br>
                    <div class="mover">
                        <select class="form-control mover" name="edad" id="edad" required autofocus>
                            <option value="" selected>Cambiar Pegi</option>
                            <option value="1">+18
                            <option value="2">+16
                            <option value="3">+12
                            <option value="4">+7
                            <option value="5">+3
                        </select>
                    </div>
                    <br>
                    <select class="form-control" name="plataforma" id="plataforma" required autofocus>
                        <option value="" selected>Cambiar Plataforma</option>
                        <option value="1">Steam
                        <option value="2">Origin
                        <option value="3">Uplay
                        <option value="4">Battle.net
                        <option value="5">PS4
                        <option value="6">Nintendo
                    </select>
                    <br>
                    <div align="center" class="disponible">
                        <select class="form-control" id="disponible" name="disponible">
                            <option value="" selected>Cambiar Disponible</option>
                            <option value="1">Si
                            <option value="0">No
                        </select>
                    </div>
                    <br>
                    <div align="center" class="disponible">
                        <select class="form-control" id="dlc" name="dlc">
                            <option value="" selected>Cambiar Dlc</option>
                            <option value="1">Si
                            <option value="0">No
                        </select>
                    </div>
                    <br>
                    <select class="form-control" id="id_juego" name="id_juego" required autofocus>
                        <option value="" selected>Seleccionar Juego</option>
                        <?php
                        $sqlUsuario="select id, titulo from juegos";
                        $resultadoUsuario=$db->lanzar_consulta($sqlUsuario);
                        while($filaUsuario = $resultadoUsuario->fetch_assoc()){
                            ?>
                            <option value="<?=$filaUsuario["id"] ?>"><?=$filaUsuario["titulo"]?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <br>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace("descripcion");
</script>