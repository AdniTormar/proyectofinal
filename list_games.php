<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();
if (isset($_REQUEST["id"]))
    $id = $_REQUEST["id"];
else if(!(isset($_REQUEST["id"]))){
    $id = "view_game";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
</head>
<body>
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
        include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        $estaenjuego=true;
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>
<div class="div_jue">
    <table cellspacing="3" cellpadding="3">
        <tr>
            <?php
            include($id.".php");
            ?>
        </tr>
    </table>
</div>
<!-- Abajo -->
<div class="container">
    <?php
    if(isset($contador)){
    ?>
    <div align="center" class="<?php if ($contador <= 4 && $contador < 5) { ?>abajo9<?php } ?><?php if ($contador >= 5 && $contador < 9) { ?>abajo7<?php } ?><?php if ($contador >= 9) { ?>abajo8<?php } ?>">
        <?php
        }
        if(!(isset($contador))) {
            ?>
        <div align="center" class="abajoJ">
            <?php
        }
        ?>
        <?php
            include("pie.php");
        ?>
    </div>
</div>
<?php
$db->desconectar();
?>
</body>
</html>
