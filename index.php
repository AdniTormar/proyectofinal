<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();
if(isset($_REQUEST["mensaje"]))
    $mensaje=$_REQUEST["mensaje"];
else
    $mensaje="";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>
</head>
<body onmouseover="CambiarCur()" id="body">
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
        include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>
<div id="carouselExampleIndicators" class="carousel slide fondo" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="" id="elem"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="" id="elem1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="" id="elem2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3" class="" id="elem3"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="4" class="" id="elem4"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
           <a id="enlace" href=""><img id="imagen" class="d-block w-100 fondo"></a>
            <marquee direction="left" scrolldelay="20" truespeed  loop="1" behavior="slide" class="Letra">
                <p align="center" class="CambiarC">
                    <strong>
                    En esta página podras encontrar todo tipo de videojuegos para ordenadores, para plataformas para steam, origin,
                    gog y uplay.<br> Para más informacion pinche
                    <a href="info.php" class="EnlaceAbajo">
                        aqui
                    </a>.</strong>
                </p>
            </marquee>
        </div>
        <button onclick="boton2()" class="esconder">
            <a class="carousel-control-prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
        </button>
        <button onclick="boton1()" class="esconder">
            <a class="carousel-control-next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </button>
    </div>
</div>
<?php
if($mensaje != ""){
    ?>
    <div class="alert alert-danger abajo" align="center">
        <?=base64_decode($mensaje) ?>
    </div>
    <?php
}
?>
<!-- Abajo -->
<div class="container">
    <div align="center" class="abajo">
        <?php
            include("pie.php");
        ?>
    </div>
</div>
<?php
$db->desconectar();
?>
</body>
</html>