<?php
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();

session_start();
session_abort();
if(isset($_REQUEST["mensaje"]))
    $mensaje=$_REQUEST["mensaje"];
else
    $mensaje="";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOG</title>
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" type="image/gif" href="img/icono.png" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="js/principal.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
    <SCRIPT src="libs/ckeditor/ckeditor.js"></SCRIPT>
</head>
<body onmouseover="CambiarCur()" id="body">
<!-- Arriba -->
<nav class="navbar navbar-expand-lg">
    <?php
        include("icono.php");
    ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        include("header.php");
        ?>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>-->
    </div>
</nav>
<!-- Medio -->
<?php
include("lateral.php");
?>
<div class="container">
    <form class="form-signin centrar2" method="post" action="create_game.php" enctype="multipart/form-data">
        <h2 class="form-signin-heading" align="center">Crear Juego</h2>
        <br>
        <label for="inputEmail" class="sr-only">Titulo</label>
        <input type="text" id="titulo" name="titulo" class="form-control" placeholder="Titulo" maxlength="30" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Descripción</label>
        <textarea name="descripcion" class="form-control" id="descripcion" placeholder="Descripcion" required autofocus></textarea>
        <br>
        <label for="inputPassword" class="sr-only">Precio</label>
        <input type="text" id="precio" name="precio" class="form-control" placeholder="Precio" maxlength="6" required autofocus>
        <br>
        <label for="inputPassword" class="sr-only">Empresa</label>
        <input type="text" id="empresa" name="empresa" class="form-control" placeholder="Empresa" maxlength="20" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Fecha Lanzamiento</label>
        <input type="text" id="datepicker" name="fecha" class="form-control" placeholder="Fecha Lanzamiento" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Foto Juego</label>
        <input type="file" id="foto1" name="foto1" class="form-control" placeholder="Foto Juego" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Foto Fondo</label>
        <input type="file" id="foto2" name="foto2" class="form-control" placeholder="Foto Fondo" required autofocus>
        <br>
        <label for="inputEmail" class="sr-only">Video</label>
        <input type="file" id="video" name="video" class="form-control" placeholder="Video" required autofocus>
        <br>
        <div class="mover2">
            <label><strong class="mover">Genero</strong></label>
            <label><strong class="mover2">Edad</strong></label>
            <select class="form-control mover" name="genero" id="genero" required autofocus>
                <option selected value="1">Acción
                <option value="2">Arcade
                <option value="3">Aventura
                <option value="4">Carreras
                <option value="5">Cooperación
                <option value="6">Multijugador
                <option value="7">Deporte
                <option value="8">Estrategia
                <option value="9">FPS
                <option value="10">Indies
                <option value="11">Lucha
                <option value="12">MMO
                <option value="13">Simulación
                <option value="14">ROL
                <option value="15">Misterio
                <option value="16">RPG
                <option value="17">Mundo Abierto
            </select>
        </div>
        <div class="mover">
            <select class="form-control mover" name="edad" id="edad" required autofocus>
                <option selected value="1">+18
                <option value="2">+16
                <option value="3">+12
                <option value="4">+7
                <option value="5">+3
            </select>
        </div>
        <select class="form-control" name="plataforma" id="plataforma" required autofocus>
            <option selected value="1">Steam
            <option value="2">Origin
            <option value="3">Uplay
            <option value="4">Battle.net
            <option value="5">PS4
            <option value="6">Nintendo
        </select>
        <br>
        <div align="center" class="disponible">
            <label class="sr-only">Disponible</label>
            <input class="form-check-input" name="disponible" id="disponible" type="checkbox"> Disponible
        </div>
        <br>
        <div align="center" class="disponible">
            <label class="sr-only">DLC</label>
            <input class="form-check-input" name="dlc" id="dlc" type="checkbox"> DLC
        </div>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
        <br>
        <?php
        if($mensaje != ""){
            ?>
            <div class="alert alert-danger" align="center">
                <?=$mensaje ?>
            </div>
            <?php
        }
        ?>
    </form>
</div> <!-- /container -->
<div "container">
<div align="center" class="abajo4">
    <?php
        include("pie.php");
    ?>
</div>
</div>
<script>
    CKEDITOR.replace("descripcion");
</script>
<?php
$db->desconectar();
?>
</body>
</html>