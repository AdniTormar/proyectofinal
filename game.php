<?php
$id_juego = $_REQUEST["id_juego"];

$sql = "SELECT * FROM juegos WHERE id = ?";
$resultado=$db->lanzar_consulta($sql, array($id_juego));
$fila = $resultado->fetch_assoc();
?>
<td class="game">
    <div class="card game">
        <img class="card-img-top" style=""src="img/<?=$fila['foto_fondo']?>" alt="Card image cap">
        <div class="card-body">
            <h4 class="card-title"><?=$fila["titulo"] ?></h4>
            <p class="card-text">Descripcion: </p>
            <p class="card-text"><?=$fila['descripcion']?></p>
            <p class="card-text game"><small class="text-muted"> Empresa: <?= $fila["empresa"] ?> Genero: <?= $fila["genero"] ?> Pegi: <?= $fila["pegi"] ?><br> Fecha Lanzamiento: <?= $fila["fecha"] ?> Precio: <?= $fila["precio"] ?></small></p>
            <a href="" class="btn btn-primary trailer" data-toggle="modal" data-target="#miModal">Ver trailer</a>
            <?php if(isset($_SESSION["id"])||isset($_SESSION["idCo"])){ ?>
                <a href="enviar_cesta.php?id_titulo=<?=$fila['titulo'] ?>&id_pegi=<?=$fila['pegi'] ?>&id_empresa=<?=$fila['empresa'] ?>&id_foto_ver=<?=$fila['foto_ver'] ?>&id_precio=<?=$fila['precio'] ?>&id_juego=<?=$fila['id'] ?>"class="btn btn-primary carro">Añadir al carro</a>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog vmodal2" role="document">
            <div class="modal-content ventanamodal">
                <div class="modal-body">
                    <video src="videos/<?=$fila["video"]?>" class="video" controls></video>
                </div>
            </div>
        </div>
    </div>
</td>