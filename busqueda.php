<?php
define("TAMANO_PAGINA2", 12);

if(isset($_REQUEST["pagina"])){
    $pagina=$_REQUEST["pagina"];
}
else{
    $pagina = 0;
}
$busqueda =  "%" . $_REQUEST["cajabusqueda"] . "%" ;
if(isset($_REQUEST["cajabusqueda2"])){
    $minisculas=strtolower($_REQUEST["cajabusqueda2"]);
    //var_dump($minisculas);
    $acentos=explode("cci",$minisculas);
    $acentos2=explode("oop",$minisculas);
    $acentos3=explode("imu",$minisculas);
    if($acentos[0]=="a"){
        $minisculas="accion";
    }
    if($acentos2[0]=="c"){
        $minisculas="cooperacion";
    }
    if($acentos3[0]=="s"){
        $minisculas="simulacion";
    }
    //var_dump($minisculas);
    $minisculas=base64_encode($minisculas);
    if($minisculas==base64_encode("accion")){
        $minisculas="accion";
    }
    if($minisculas==base64_encode("cooperacion")){
        $minisculas="cooperacion";
    }
    if($minisculas==base64_encode("simulacion")){
        $minisculas="simulacion";
    }
    if($minisculas==base64_encode("arcade")){
        $minisculas="arcade";
    }
    if($minisculas==base64_encode("aventura")){
        $minisculas="aventura";
    }
    if($minisculas==base64_encode("carreras")){
        $minisculas="carreras";
    }
    if($minisculas==base64_encode("multijugador")){
        $minisculas="multijugador";
    }
    if($minisculas==base64_encode("deporte")){
        $minisculas="deporte";
    }
    if($minisculas==base64_encode("estrategia")){
        $minisculas="estrategia";
    }
    if($minisculas==base64_encode("fps")){
        $minisculas="fps";
    }
    if($minisculas==base64_encode("indies")){
        $minisculas="indies";
    }
    if($minisculas==base64_encode("lucha")){
        $minisculas="lucha";
    }
    if($minisculas==base64_encode("mmo")){
        $minisculas="mmo";
    }
    if($minisculas==base64_encode("rol")){
        $minisculas="rol";
    }
    if($minisculas==base64_encode("misterio")){
        $minisculas="misterio";
    }
    if($minisculas==base64_encode("rpg")){
        $minisculas="rpg";
    }
    if($minisculas==base64_encode("mundo abierto")){
        $minisculas="mundo abierto";
    }
    $busqueda2 =  "%" . $minisculas  . "%" ;
}
else{
    $busqueda2=null;
}
if(isset($_REQUEST["cajabusqueda3"])){
    $busqueda3 = $_REQUEST["cajabusqueda3"];
}
else{
    $busqueda3=null;
}
if(!(isset($_REQUEST["cajabusqueda4"]))){
    $busqueda4=0;
}
else{
    $busqueda4=1;
}
//var_dump($busqueda3);
if($_REQUEST["cajabusqueda"]==""||$_REQUEST["cajabusqueda"]==null){
    header("location: list_games.php");
}
else{
    if(isset($busqueda)&&!isset($busqueda2)&&!isset($busqueda3)&&!isset($busqueda4)){
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda));
    }
    else if(isset($busqueda)&&isset($busqueda2)&&isset($busqueda3)&&isset($busqueda4)){
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ? and genero LIKE ? and plataforma=? and dlc=?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda,$busqueda2,$busqueda3,$busqueda4));
    }
    else if(isset($busqueda)&&isset($busqueda2)&&!isset($busqueda3)&&isset($busqueda4)) {
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ? and genero LIKE ? and dlc=?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda,$busqueda2,$busqueda4));
    }
    else if(isset($busqueda)&&!isset($busqueda2)&&isset($busqueda3)&&isset($busqueda4)) {
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ? and plataforma=? and dlc=?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda,$busqueda3,$busqueda4));
    }
    else if(isset($busqueda)&&isset($busqueda2)&&isset($busqueda3)&&!isset($busqueda4)){
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ? and genero LIKE ? and plataforma=?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda,$busqueda2,$busqueda3));
    }
    else if(isset($busqueda)&&!isset($busqueda2)&&!isset($busqueda3)&&isset($busqueda4)){
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ? and dlc=?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda,$busqueda4));
    }
    else if(isset($busqueda)&&!isset($busqueda2)&&isset($busqueda3)&&!isset($busqueda4)){
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ? and plataforma=?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda,$busqueda3));
    }
    else if(isset($busqueda)&&isset($busqueda2)&&!isset($busqueda3)&&!isset($busqueda4)){
        $sql3="select COUNT(*) as 'cantidad' from juegos where disponible=? and titulo LIKE ? and genero LIKE ?";
        $resultado3=$db->lanzar_consulta($sql3,array(1,$busqueda,$busqueda2));
    }
    //var_dump($sql3);
    $fila3=$resultado3->fetch_assoc();
    $entradas=$fila3["cantidad"];
    $paginas=$entradas / TAMANO_PAGINA2;
    $contador=0;
    if(isset($busqueda)&&!isset($busqueda2)&&!isset($busqueda3)&&!isset($busqueda4)){
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda));
    }
    if(isset($busqueda)&&isset($busqueda2)&&isset($busqueda3)&&isset($busqueda4)) {
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? and genero LIKE ? and plataforma=? and dlc=? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda, $busqueda2, $busqueda3, $busqueda4));
    }
    if(isset($busqueda)&&isset($busqueda2)&&!isset($busqueda3)&&isset($busqueda4)) {
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? and genero LIKE ? and dlc=? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda, $busqueda2, $busqueda4));
    }
    if(isset($busqueda)&&!isset($busqueda2)&&isset($busqueda3)&&isset($busqueda4)) {
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? and plataforma=? and dlc=? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda, $busqueda3, $busqueda4));
    }
    if(isset($busqueda)&&!isset($busqueda2)&&isset($busqueda2)&&isset($busqueda3)) {
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? and genero LIKE ? and plataforma=? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda, $busqueda2, $busqueda3));
    }
    if(isset($busqueda)&&!isset($busqueda2)&&!isset($busqueda3)&&isset($busqueda4)) {
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? and dlc=? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda, $busqueda4));
    }
    if(isset($busqueda)&&isset($busqueda2)&&!isset($busqueda3)&&!isset($busqueda4)) {
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? and genero LIKE ? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda, $busqueda2));
    }
    if(isset($busqueda)&&!isset($busqueda2)&&isset($busqueda3)&&!isset($busqueda4)) {
        $sql = "SELECT titulo,SUBSTR(descripcion,1,30)as'descripcion',foto_ver, fecha, id,plataforma,dlc FROM juegos where disponible=? and titulo LIKE ? and plataforma=? ORDER BY fecha LIMIT " . $pagina  * TAMANO_PAGINA2 . ", " . TAMANO_PAGINA2;
        $resultado = $db->lanzar_consulta($sql, array(1, $busqueda, $busqueda3));
    }
    //var_dump($sql);
    while($fila=$resultado->fetch_assoc()){
        ?>
        <?php
        $contador++;
        ?>
        <?php
        if($contador==13) {
            $contador=0;
        }
        if($contador<=4) {
            ?>
            <td>
                <div>
                    <div class="card juego">
                        <img class="card-img-top ver" src="img/<?= $fila['foto_ver'] ?>" alt="Card image cap">
                        <?php
                        if($fila["plataforma"]=="steam"){
                            ?>
                            <img class="card-img-top ver2" src="img/steam.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="origin"){
                            ?>
                            <img class="card-img-top ver2" src="img/origin.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="uplay"){
                            ?>
                            <img class="card-img-top ver2" src="img/uplay.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="battle.net"){
                            ?>
                            <img class="card-img-top ver2" src="img/battle.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="ps4"){
                            ?>
                            <img class="card-img-top ver2" src="img/ps4.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="nintendo"){
                            ?>
                            <img class="card-img-top ver2" src="img/nintendo.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["dlc"]==1){
                            ?>
                            <img class="card-img-top ver3" src="img/dlc.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <div class="card-body">
                            <h4 class="card-title"><?= $fila["titulo"] ?></h4>
                            <p class="card-text"><?= $fila['descripcion'] ?>...</p>
                            <a href="?id=game&id_juego=<?= $fila["id"] ?><?= $fila["titulo"] ?>" class="btn btn-primary">Leer
                                más</a>
                        </div>
                    </div>
                </div>
            </td>
            <?php
        }
        ?>
        <?php
        if($contador>=5) {
            ?>
            <td>
                <div>
                    <div class="card <?php if($contador==5){?>juego2<?php } ?><?php if($contador==6){?>juego3<?php } ?><?php if($contador==7){?>juego4<?php } ?><?php if($contador==8){?>juego5<?php } ?><?php if($contador==9){?>juego6<?php } ?><?php if($contador==10){?>juego7<?php } ?><?php if($contador==11){?>juego8<?php } ?><?php if($contador==12){?>juego9<?php } ?>">
                        <img class="card-img-top ver" src="img/<?= $fila['foto_ver'] ?>" alt="Card image cap">
                        <?php
                        if($fila["plataforma"]=="steam"){
                            ?>
                            <img class="card-img-top ver2" src="img/steam.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="origin"){
                            ?>
                            <img class="card-img-top ver2" src="img/origin.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="uplay"){
                            ?>
                            <img class="card-img-top ver2" src="img/uplay.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="battle.net"){
                            ?>
                            <img class="card-img-top ver2" src="img/battle.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="ps4"){
                            ?>
                            <img class="card-img-top ver2" src="img/ps4.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["plataforma"]=="nintendo"){
                            ?>
                            <img class="card-img-top ver2" src="img/nintendo.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <?php
                        if($fila["dlc"]==1){
                            ?>
                            <img class="card-img-top ver3" src="img/dlc.png" alt="Card image cap">
                            <?php
                        }
                        ?>
                        <div class="card-body">
                            <h4 class="card-title"><?= $fila["titulo"] ?></h4>
                            <p class="card-text"><?= $fila['descripcion'] ?>...</p>
                            <a href="?id=game&id_juego=<?= $fila["id"] ?><?= $fila["titulo"] ?>"
                               class="btn btn-primary">Leer
                                más</a>
                        </div>
                    </div>
                </div>
            </td>
            <?php
        }
    }
    ?>
    <nav class="<?php if($contador<=4 && $contador<5){?>paginacion3<?php } ?><?php if($contador>=5 && $contador<9){?>paginacion2<?php } ?><?php if($contador>=9){?>paginacion<?php } ?>" aria-label="Page navigation example">
        <ul class="pagination">
            <?php
            $porcentaje=explode("%",$busqueda2);
            $busqueda2=$porcentaje[1];
            for ($i = 0;$i<$paginas;$i++){
                ?>
                <li class="page-item"><a class="page-link" href="?cajabusqueda=<?=$_REQUEST["cajabusqueda"];?>&cajabusqueda2=<?=$busqueda2;?><?php if(!($busqueda3==null)){?>&cajabusqueda3=<?=$busqueda3;}?><?php if(isset($busqueda4)==0||isset($busqueda4)==1){?>&cajabusqueda4=<?=$busqueda4;}?>&id=busqueda&pagina=<?=$i ?>" > <?=$i + 1?> </a></li>
                <?php
            }
            ?>
            <li class="page-item"><a class="page-link" > ... </a></li>
        </ul>
    </nav>
    <?php
}
?>