<?php

include("conf/configuracion.php");
include("funcion/funcion.php");

$db=new Db();
$db->conectar();

session_start();
session_abort();

$sql2="select now()";
$resultado2=$db->lanzar_consulta($sql2);
$fila2=$resultado2->fetch_row();

$id_juego=$_REQUEST["id_juego"];

$id_producto=$_REQUEST["id_titulo"];
$id_edad=$_REQUEST["id_pegi"];
$id_edad=str_replace(" ","+",$id_edad);
$id_empresa=$_REQUEST["id_empresa"];
$id_precio=$_REQUEST["id_precio"];
$id_foto_ver=$_REQUEST["id_foto_ver"];
$id_fecha_compra = date("Y-m-d H:i:s", strtotime($fila2[0]));


/*var_dump($id_producto);
var_dump($id_edad);
var_dump($id_empresa);
var_dump($id_precio);
var_dump($id_foto_ver);
var_dump($id_fecha_compra);
var_dump($id_juego);
exit();*/

if(isset($id_producto)||isset($id_edad)||isset($id_empresa)||isset($id_precio)||isset($id_foto_ver)||isset($id_fecha_compra)||isset($id_juego)){
    if(isset($_SESSION["id"])){
        $sql="insert into cesta (juego,pegi,empresa,precio,foto_ver,fecha_cesta,id_usuario) VALUES (?,?,?,?,?,?,?)";
        $resultado=$db->lanzar_consulta($sql, array($id_producto,$id_edad,$id_empresa,$id_precio,$id_foto_ver,$id_fecha_compra,$_SESSION["id"]));

        $sqlc = ("select id as 'id',juego as 'juego',id_usuario as 'comprobar' from cesta where juego=? and id_usuario=?");

        $resultadoc = $db->lanzar_consulta($sqlc, array($id_producto,$_SESSION["id"]));
        while($filac = $resultadoc->fetch_assoc()) {
            if ($_SESSION["id"] == $filac["comprobar"]) {
                if($id_producto==$filac["juego"]) {
                    $sql3 = "insert into juegos_cesta (id_juego, id_cesta) VALUES (?,?)";
                    $resultado3 = $db->lanzar_consulta($sql3, array($id_juego, $filac["id"]));
                    $db->desconectar();
                    header("Location: cesta.php");
                }
            }
        }
    }
    if(isset($_SESSION["idCo"])){
        $sql="insert into cesta (juego,pegi,empresa,precio,foto_ver,fecha_cesta,id_usuario) VALUES (?,?,?,?,?,?,?)";
        $resultado=$db->lanzar_consulta($sql, array($id_producto,$id_edad,$id_empresa,$id_precio,$id_foto_ver,$id_fecha_compra,$_SESSION["idCo"]));
        $sqlc = ("select id as 'id',juego as 'juego',id_usuario as 'comprobar' from cesta where juego=? and id_usuario=?");

        $resultadoc = $db->lanzar_consulta($sqlc, array($id_producto,$_SESSION["idCo"]));
        while($filac = $resultadoc->fetch_assoc()) {
            if ($_SESSION["idCo"] == $filac["comprobar"]) {
                if($id_producto==$filac["juego"]) {
                    $sql3 = "insert into juegos_cesta (id_juego, id_cesta) VALUES (?,?)";
                    $resultado3 = $db->lanzar_consulta($sql3, array($id_juego, $filac["id"]));
                    $db->desconectar();
                    header("Location: cesta.php");
                }
            }
        }
    }
}
else{
    $db->desconectar();
    header("Location: index.php");
}

?>