<?php
define("TAMANO_PAGINA", 9);
include("conf/configuracion.php");
include("funcion/funcion.php");
$db=new Db();
$db->conectar();
session_start();
session_abort();

if(isset($_REQUEST["pagina"])){
    $pagina=$_REQUEST["pagina"];
}
else{
    $pagina = 0;
}

$sql3="select COUNT(*) as 'cantidad' from comentarios";
$resultado3=$db->lanzar_consulta($sql3);
$fila3=$resultado3->fetch_assoc();
$entradas=$fila3["cantidad"];
$paginas=$entradas / TAMANO_PAGINA;

$sqlc = "SELECT c.juego,c.comentario,c.valoracion,c.foto_ver,c.fecha_compra,c.id_usuario,c.id_juego,u.id,u.usuario  from usuarios u,comentarios c where u.id=c.id_usuario ORDER BY fecha_compra ASC LIMIT " . $pagina  * TAMANO_PAGINA . ", " . TAMANO_PAGINA;
$resultadoc=$db->lanzar_consulta($sqlc);
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
        <title>JOG</title>
        <link rel="stylesheet" href="css/principal.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="icon" type="image/gif" href="img/icono.png" />
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="js/principal.js"></script>
    </head>
    <body onmouseover="CambiarCur()" id="body">
    <!-- Arriba -->
    <nav class="navbar navbar-expand-lg">
        <?php
        include("icono.php");
        ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php
            include("header.php");
            ?>
            <!--<form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>-->
        </div>
    </nav>
    <!-- Medio -->
    <?php
    include("lateral.php");
    ?>
    <div class="div_jue">
        <table cellspacing="3" cellpadding="3">
            <tr>
            <?php
            $contador=0;
            while($filac=$resultadoc->fetch_assoc()){
                ?>
                <?php
                $contador++;
                if($contador<=3) {
                    ?>
                    <td>
                        <div align="center" class="card juegoComen" style="width: 20rem;height: 15rem;">
                            <div class="card-body">
                                <img class="card-img-top verComen" src="img/<?= $filac['foto_ver'] ?>"
                                     alt="Card image cap">
                                <h4 class="card-title"><?= $filac["juego"] ?></h4>
                                <p class="card-text">Usuario: <?= $filac['usuario'] ?>
                                    Valoración: <?= $filac['valoracion'] ?> Fecha
                                    Compra: <?= $filac["fecha_compra"] ?></p>
                                <p class="card-text">
                                    <small class="text-muted"> Comentario: <?= $filac["comentario"] ?></small>
                                </p>
                            </div>
                        </div>
                    </td>
                    <?php
                }
                if($contador>=4) {
                    ?>
                    <td>
                        <div align="center" class="card <?php if($contador==4){?>juegoComen2<?php } ?><?php if($contador==5){?>juegoComen3<?php } ?><?php if($contador==6){?>juegoComen4<?php } ?><?php if($contador==7){?>juegoComen5<?php } ?><?php if($contador==8){?>juegoComen6<?php } ?><?php if($contador==9){?>juegoComen7<?php } ?>" style="width: 20rem;height: 15rem;">
                            <div class="card-body">
                                <img class="card-img-top verComen" src="img/<?= $filac['foto_ver'] ?>"
                                     alt="Card image cap">
                                <h4 class="card-title"><?= $filac["juego"] ?></h4>
                                <p class="card-text">Usuario: <?= $filac['usuario'] ?>
                                    Valoración: <?= $filac['valoracion'] ?> Fecha
                                    Compra: <?= $filac["fecha_compra"] ?></p>
                                <p class="card-text">
                                    <small class="text-muted"> Comentario: <?= $filac["comentario"] ?></small>
                                </p>
                            </div>
                        </div>
                    </td>
                <?php
                }
            }
            ?>
            </tr>
        </table>
    </div>
    <nav aria-label="Page navigation example" style="position: absolute;top:920px;left:725px;">
        <ul class="pagination">
            <?php
            for ($i = 0;$i<$paginas;$i++){
                ?>
                <li class="page-item"><a class="page-link" href="?pagina=<?=$i ?>" > <?=$i + 1?> </a></li>
                <?php
            }
            ?>
            <li class="page-item"><a class="page-link" > ... </a></li>
        </ul>
    </nav>
    <!-- Abajo -->
    <div class="container">
        <div align="center" class="abajo2">
            <?php
            include("pie.php");
            ?>
        </div>
    </div>
    <?php
    $db->desconectar();
    ?>
    </body>
    </html>