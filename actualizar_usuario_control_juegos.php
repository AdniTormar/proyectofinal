<?php
include("conf/configuracion.php");
include("funcion/funcion.php");

$db=new Db();
$db->conectar();
session_start();
session_abort();

$titulo = $_POST["titulo"];
$descripcion = $_POST["descripcion"];
$precio = $_POST["precio"];
$empresa = $_POST["empresa"];

$foto1 = $_FILES["foto1"]["name"];
$tmp_foto1 = $_FILES["foto1"]["tmp_name"];

$foto2 = $_FILES["foto2"]["name"];
$tmp_foto2 = $_FILES["foto2"]["tmp_name"];

$video = $_FILES["video"]["name"];
$tmp_video = $_FILES["video"]["tmp_name"];

$ruta_foto1="img/".$foto1;
move_uploaded_file($tmp_foto1, $ruta_foto1);

$ruta_foto2="img/".$foto2;
move_uploaded_file($tmp_foto2, $ruta_foto2);

$ruta_video="videos/".$video;
move_uploaded_file($tmp_video, $ruta_video);

$genero = $_POST["genero"];
$edad = $_POST["edad"];
$plataforma= $_POST["plataforma"];
$dlc=$_POST["dlc"];
$disponible = $_POST["disponible"];
$id_juego = $_POST["id_juego"];

if($titulo==null||$descripcion==null||$precio==null||$empresa==null||$foto1==null||$foto2==null||$video==null||$genero==null||$edad==null||$plataforma==null||$dlc==null||$disponible==null||$id_juego==null){
    $db->desconectar();
    header("Location: panel_control.php");
}

$sql2="update juegos set titulo='$titulo',descripcion='$descripcion',precio='$precio',empresa='$empresa',foto_ver='$foto1',foto_fondo='$foto2',video='$video',genero='$genero',pegi='$edad',plataforma='$plataforma',dlc='$dlc',disponible='$disponible' where id=?";
$resultado2=$db->lanzar_consulta($sql2, array($id_juego));

$db->desconectar();
$mensaje="Se han cambiado correctamente.";
$mensaje= base64_encode($mensaje);
header("location: panel_control.php?juegos=juegos&mensaje=".$mensaje);
?>